/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import main.About;
import main.Controller;
import shop.ShopController.ShopState;


/**
 * HeaderController.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 21 Oct 2018
 *
 */
public class HeaderController implements Controller<HeaderModel>{

	@FXML private Label title, state, current, total;
	@FXML private VBox infoPane, statePane;


	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	@FXML
	protected void initialize() {

		statePane.prefWidthProperty().bind(infoPane.widthProperty());
	}

	@Override
	public void setModel(final HeaderModel model){

		title.setText(model.getTitle());
		title.setOnMouseClicked(e->  about());

		//set initial state
		state.setText(getButtonText(model.shopStateProperty().get()));
		//keep it updated
		model.shopStateProperty().addListener((obs, oldValue, newValue )->{
			if(newValue != null) {
				state.setText(getButtonText(newValue));
			}
		});

		model.accumulatedCustomers().addListener((obs, oldValue, newValue )->{
			if(newValue != null) {
				total.setText(String.valueOf(newValue));
			}
		});

		model.numOfShopCustomers().addListener((obs, oldValue, newValue )->{
			if(newValue != null) {
				current.setText(String.valueOf(newValue));
			}
		});
	}

	/**
	 *
	 */
	private void about() {

		final String space = " ", newLine = "\n";
		final Alert alert = new Alert(AlertType.NONE);
		alert.setTitle("About");

		final StringBuilder sb = new StringBuilder(About.getAppName());
		sb.append(space).append(About.getVer())
		   .append(newLine).append("Author: ").append(About.getAuthor());
		alert.setContentText(sb.toString());
		alert.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
		alert.getDialogPane().getStylesheets().add( getClass().getResource("xml/alert.css").toExternalForm());
		alert.show();
	}

	/**
	 *
	 */
	private String getButtonText(final ShopState state) {

		return state.toString().replaceAll(" ","\n");
	}
}
