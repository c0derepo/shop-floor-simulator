/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package view;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import shop.ShopController.ShopState;

/**
 * FloorModel.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 22 Oct 2018
 *
 */
interface FloorModel {

	/**
	 *
	 */
	SimpleIntegerProperty getNumOfFloorCustomers();

	/**
	 *
	 */
	int getShopCapcity();

	/**
	 *
	 */
	SimpleObjectProperty<ShopState> shopStateProperty();
}
