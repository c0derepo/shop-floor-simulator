/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package view;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.About;
import main.About.Mode;
import main.Config;
import main.Utils;
import shop.ShopController;


/**
 * MainView.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 20 Oct 2018
 *
 */
public class MainView extends Application{

	/**
	 *
	 */
	private FXMLLoader loader;

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(final Stage stage) throws Exception {

		About.setMode(Utils.isTestFolderExist() ? Mode.DEV : Mode.USE); //set use or development mode
		final Config config = new Config();
		config.setShopName(About.getAppName());
		final ShopController shop = new ShopController(config);

		final ViewModelAdaptor model = new ViewModelAdaptor(shop.getBufferQueue(),  config, shop.getCheckouts());
		model.setOpenCloseDoor(()-> shop.toggleShopState());
		model.setCloseShop(()->shop.closeShop());
		model.setNumOfFloorCustomers(shop.getCustomers().numOfFloorCustomers());
		model.setShopStateProperty(shop.shopStateProperty() );
		model.setAccumulatedNumOfCustomers(shop.accumulatedNumOfCustomers());
		model.setNumOfShopCustomers(shop.numOfShopCustomers());

		loader = new FXMLLoader();
		final Parent root = loader.load(getClass().getResource("xml/root.fxml")
 				     .openStream());/** openStream() is essential to get controller  **/
		setModel(model);

		stage.setScene(new Scene(root));
		stage.setResizable(false);
		stage.show();

		stage.setOnCloseRequest(t -> {
		    Platform.exit();
		    System.exit(0);
		});
	}

	/**
	 *
	 *@param model
	 */
	private void setModel(final ViewModel model){

    	final RootController controller = loader.getController();
		controller.setModel(model);
	}
}
