/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package view;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import main.Controller;
import main.NodeFromXml;
import progress_bar.ProgressbarFx;
import progress_bar.ProgressbarFxModel;
import progress_bar.ProgressbarModel.Progress;
import shop.Checkout;

/**
 * CheckoutsController.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 21 Oct 2018
 *
 */
public class CheckoutsController implements Controller<CheckoutsModel> {

	@FXML  private VBox checkoutPane;
	@FXML  private HBox bufferCountPane;
	@FXML  private TilePane queuesPane;

	/**
	 *
	 */
	private ProgressbarFx bufferQueueCustomersBar;

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	@FXML
	public void initialize(){

		bufferQueueCustomersBar = new ProgressbarFx();
	}

	@Override
	public void setModel(final CheckoutsModel model){

		final ProgressbarFxModel barModel = new ProgressbarFxModel
								(model.getNumOfBufferCustomers(),model.getBufferQueueCapcity() );
		barModel.setProgressDir(Progress.LEFT)
      			.setCssUrl(getClass().getResource("xml/checkoutsBufferProgressBar.css"));
		bufferQueueCustomersBar.setModel(barModel);

		try {
			bufferCountPane.getChildren().add(bufferQueueCustomersBar.makeProgressBar());
		} catch (final IOException ex) {
			ex.printStackTrace();
		}

		final ProgressBar progressBar = bufferQueueCustomersBar.getProgressBar();
		progressBar.prefWidthProperty().bind(bufferCountPane.widthProperty()
								.subtract(bufferCountPane.getPadding().getLeft())
								.subtract(bufferCountPane.getPadding().getRight()));
		progressBar.setPrefHeight(26);

		queuesPane.setPrefColumns(model.getNumbrOfQueues());

		for (final Checkout checkout : model.getCheckouts()) {

			NodeFromXml<CheckoutModel> quePane = null;
			try {
				quePane = new NodeFromXml<>(getClass().getResource("xml/queue.fxml"));
			} catch (final IOException ex) {
				ex.printStackTrace();
			}
			final CheckOutModelAdapter adapter = new CheckOutModelAdapter(checkout);
			adapter.setOptimalCapcity(model.getOptimalCapcity());
			quePane.setModel(adapter);
			queuesPane.getChildren().add(quePane.getNode());
		}
	}
}
