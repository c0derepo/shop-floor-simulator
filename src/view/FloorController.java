/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package view;

import java.io.IOException;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import main.Controller;
import progress_bar.ProgressbarFx;
import progress_bar.ProgressbarFxModel;
import shop.ShopController.ShopState;

/**
 * FloorController.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 21 Oct 2018
 *
 */
public class FloorController implements Controller<FloorModel>{

	/**
	 *
	 */
	@FXML private HBox floorCountPane, middleDoor;
	@FXML private VBox door;

	/**
	 *
	 */
	private ProgressbarFx shopCustomersBar;

	/**
	 *
	 */
	private ProgressBar progressBar;

	/**
	 *
	 */
	private PseudoClass lowStyle, mediumStyle, highStyle;
	private static final String LOW  = "low-bar", MEDIUM  = "medium-bar", HIGH = "high-bar";

	/**
	 * Low range and medium range limits on a 0-1 scale
	 */
	private static final double LOW_RANGE = .331, MID_RANGE = 0.661;

	/**
	 * Door size for animation
	 */
	private static final int MIN_DOOR_SIZE = 0, MAX_DOOR_SIZE = 80;

	/**
	 * Door animation time line
	 */
	private Timeline timeline;

	/**
	 *
	 */
	private static final Duration ANIMATION_DURATION = Duration.millis(2000);

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	@FXML
	public void initialize(){

		lowStyle = PseudoClass.getPseudoClass(LOW);
		mediumStyle = PseudoClass.getPseudoClass(MEDIUM);
		highStyle = PseudoClass.getPseudoClass(HIGH);

		shopCustomersBar = new ProgressbarFx();
	}

	@Override
	public void setModel(final FloorModel model){

		final ProgressbarFxModel barModel = new ProgressbarFxModel
										(model.getNumOfFloorCustomers(),model.getShopCapcity());

		barModel.setCssUrl(getClass().getResource("xml/floorProgressBar.css"))
				.setOnProgressChangeDo(d-> styleBar(d));
		shopCustomersBar.setModel(barModel);

		try {
			floorCountPane.getChildren().add(shopCustomersBar.makeProgressBar());
		} catch (final IOException ex) {
			ex.printStackTrace();
		}

		progressBar = shopCustomersBar.getProgressBar();
		progressBar.prefWidthProperty().bind(floorCountPane.widthProperty().subtract(door.getPrefWidth()));
		progressBar.prefHeightProperty().bind(floorCountPane.heightProperty());

		model.shopStateProperty().addListener((obs, oldValue, newValue)->{
			if(newValue == ShopState.DOORS_CLOSED){
				closeDoor();
			}else if(newValue == ShopState.OPEN){
				openDoor();
			}
		});
	}

	private void styleBar(final double progress) {

		if(lowStyle == null || mediumStyle == null || lowStyle == null)
			return ;

		progressBar.pseudoClassStateChanged (lowStyle, false);
		progressBar.pseudoClassStateChanged (mediumStyle, false);
		progressBar.pseudoClassStateChanged (highStyle, false);

		if(progress < LOW_RANGE ) {
			progressBar.pseudoClassStateChanged (lowStyle, true);
		}else if( progress < MID_RANGE) {
			progressBar.pseudoClassStateChanged (mediumStyle, true);
		}else {
			progressBar.pseudoClassStateChanged (highStyle, true);
		}
	}

	/**
	 *
	 */
	private void openDoor() {
		animateDoor(MIN_DOOR_SIZE);
	}

	/**
	 *
	 */
	private void closeDoor() {
		animateDoor(MAX_DOOR_SIZE);
	}

	/**
	 *
	 */
	private void animateDoor(final int toSize) {

		if(timeline != null) {
			timeline.stop();
		}
		timeline = new Timeline(
				new KeyFrame(ANIMATION_DURATION, new KeyValue(middleDoor.maxHeightProperty(),
																			toSize,Interpolator.EASE_BOTH))
		 );
		timeline.play();
	}
}
