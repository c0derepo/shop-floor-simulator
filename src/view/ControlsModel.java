/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package view;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import shop.ShopController.ShopState;

/**
 * ControlsModel.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 22 Oct 2018
 *
 */
interface ControlsModel {

	/**
	 *
	 */
	void openCloseDoor();

	/**
	 *
	 */
	void closeShop();

	/**
	 *
	 */
	double getSimultionSpeed();

	/**
	 *
	 */
	void setSimultionSpeed(double speed);

	/**
	 *
	 */
	SimpleBooleanProperty autoPilotProperty();

	/**
	 *
	 */
	SimpleObjectProperty<ShopState> shopStateProperty();

	/**
	 *
	 */
	boolean isDevelopMode();
}
