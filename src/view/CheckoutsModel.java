/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package view;

import java.util.List;

import javafx.beans.property.SimpleIntegerProperty;
import shop.Checkout;

/**
 * CheckoutsModel.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 22 Oct 2018
 *
 */
interface CheckoutsModel {

	List<Checkout> getCheckouts();

	SimpleIntegerProperty getNumOfBufferCustomers();

	int getBufferQueueCapcity();

	int getOptimalCapcity() ;

	/**
	 *
	 */
	default int getNumbrOfQueues() { return getCheckouts().size(); }

}
