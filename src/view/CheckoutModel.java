/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package view;

import java.util.OptionalInt;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import shop.Checkout.State;

/**
 * CheckoutModel.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 28 Oct 2018
 *
 */
public interface CheckoutModel {

	boolean isEmpty();

	boolean isFull();

	SimpleBooleanProperty isShuttingDown();

	SimpleBooleanProperty isStopping();

	int getQueueCapcity() ;

	int getOptimalCapcity() ;

	SimpleIntegerProperty getNumOfQueCustomers();

	int getQueueId() ;

	SimpleObjectProperty<State> checkOutStateProperty();

	void setCheckoutToState(State state);

	int getCheckOutId();

	SimpleObjectProperty<OptionalInt> getCustomerId();
}
