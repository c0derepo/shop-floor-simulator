/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package view;

import java.util.List;

import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import main.Config;
import shop.Checkout;
import shop.CustomerQueue;
import shop.ShopController.ShopState;

/**
 * ShopToViewConnector.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 23 Oct 2018
 *
 */
public class ViewModelAdaptor implements ViewModel{

	/**
	 *
	 */
	private final Config config;

	/**
	 *
	 */
	private Runnable openCloseDoor;

	/**
	 *
	 */
	private Runnable closeShop;

	/**
	 *
	 */
	private final List<Checkout> checkouts;

	/**
	 *
	 */
	private final CustomerQueue bufferQueue;

	/**
	 *
	 */
	private SimpleObjectProperty<ShopState> shopState;

	/**
	 *
	 */
	private final SimpleIntegerProperty numOfFloorCustomers;

	/**
	 *
	 */
	private final SimpleIntegerProperty accumulatedNumOfCustomers;

	/**
	 * Represents total shop customers: floor + queues + buffer queue
	 */
	private final SimpleIntegerProperty numOfShopCustomers;

	// ///////////////////////////////////////////////////////////
	// ////////////////// Constructor ////////////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 *
	 */
	ViewModelAdaptor(final CustomerQueue bufferQueue ,final Config config, final List<Checkout> checkouts ) {

		this.bufferQueue = bufferQueue;
		this.config = config;
		this.checkouts = checkouts;

		numOfFloorCustomers = new SimpleIntegerProperty(0);
		accumulatedNumOfCustomers = new SimpleIntegerProperty(0);
		numOfShopCustomers = new SimpleIntegerProperty(0);
	}

	/**
	* Set property to update {@link #numOfFloorCustomers}
	*/
	void setNumOfFloorCustomers(final SimpleIntegerProperty numOfFloorCustomers) {

		numOfFloorCustomers.addListener((obs,oldValue, newValue) ->{
			Platform.runLater(()-> {
				this.numOfFloorCustomers.set((int) newValue);
			});
		});
	}

	/**
	* Set property to update {@link #accumulatedNumOfCustomers}
	*/
	void setAccumulatedNumOfCustomers(final SimpleIntegerProperty accumulatedNumOfCustomers) {

		accumulatedNumOfCustomers.addListener((obs,oldValue, newValue) ->{
			Platform.runLater(()-> {
				this.accumulatedNumOfCustomers.set((int) newValue);
			});
		});
	}

	/**
	* Set property to update {@link #numOfShopCustomers}
	*/
	void setNumOfShopCustomers(final SimpleIntegerProperty numOfShopCustomers) {

		numOfShopCustomers.addListener((obs,oldValue, newValue) ->{
			Platform.runLater(()-> {
				this.numOfShopCustomers.set((int) newValue);
			});
		});
	}

	// ///////////////////////////////////////////////////////////
	// ////////////////// Getters, Setters ///////////////////////
	// ///////////////////////////////////////////////////////////

	//--Header model

	/**
	 * @see view.HeaderModel#getTitle()
	 */
	@Override
	public String getTitle() {

		return config.getShopName();
	}

	/**
	 * @see view.HeaderModel#shopStateProperty()
	 */
	@Override
	public SimpleObjectProperty<ShopState> shopStateProperty() {

		return shopState;
	}

	/**
	* Set {@link #shopState}
	*/
	void setShopStateProperty(final SimpleObjectProperty<ShopState> shopState) {
		this.shopState = shopState;
	}

	/* (non-Javadoc)
	 * @see view.HeaderModel#accumulatedCustomers()
	 */
	@Override
	public SimpleIntegerProperty accumulatedCustomers() {

		return accumulatedNumOfCustomers;
	}

	/* (non-Javadoc)
	 * @see view.HeaderModel#numOfShopCustomers()
	 */
	@Override
	public SimpleIntegerProperty numOfShopCustomers() {

		return numOfShopCustomers;
	}

	//--Floor model

	/**
	 * @see view.FloorModel#getNumOfFloorCustomers()
	 */
	@Override
	public SimpleIntegerProperty getNumOfFloorCustomers() {

		return numOfFloorCustomers;
	}

	/**
	 * @see view.FloorModel#getShopCapcity()
	 */
	@Override
	public int getShopCapcity() {

		return config.getShopCapacity();
	}

	//--Checkout model

	/**
	 * @see view.CheckoutsModel#getCheckouts()
	 */
	@Override
	public List<Checkout> getCheckouts() {

		return checkouts;
	}

	/* (non-Javadoc)
	 * @see view.CheckoutsModel#getNumOfBufferCustomers()
	 */
	@Override
	public SimpleIntegerProperty getNumOfBufferCustomers() {

		return bufferQueue.numOfQueCustomers();
	}

	/* (non-Javadoc)
	 * @see view.CheckoutsModel#getBufferQueueCapcity()
	 */
	@Override
	public int getBufferQueueCapcity() {

		return bufferQueue.getQueueCapcity();
	}

	/* (non-Javadoc)
	 * @see view.CheckoutsModel#getOptimalCapcity()
	 */
	@Override
	public int getOptimalCapcity() {

		return config.getOptimalQueueSize();
	}


	//--ControlsModel

	@Override
	public void openCloseDoor() {

		if(openCloseDoor != null) {
			openCloseDoor.run();
		}
	}

	/**
	* Set {@link #openCloseDoor}
	*/
	public void setOpenCloseDoor(final Runnable open) {

		openCloseDoor = open;
	}

	/**
	 * @see view.ControlsModel#closeShop()
	 */
	@Override
	public void closeShop() {

		if(closeShop != null) {
			closeShop.run();
		}
	}

	/**
	* Set {@link #closeShop}
	*/
	public void setCloseShop(final Runnable closeShop) {

		this.closeShop = closeShop;
	}

	/* (non-Javadoc)
	 * @see view.ControlsModel#setSimultionSpeed(double)
	 */
	@Override
	public void setSimultionSpeed(final double speed) {

		config.setSpeedFactor(speed);
	}

	/* (non-Javadoc)
	 * @see view.ControlsModel#getSimultionSpeed()
	 */
	@Override
	public double getSimultionSpeed() {

		return config.getSpeedFactor();
	}

	/* (non-Javadoc)
	 * @see view.ControlsModel#autoPilotProperty()
	 */
	@Override
	public SimpleBooleanProperty autoPilotProperty() {

		return config.autoPilotProperty();
	}

	/* (non-Javadoc)
	 * @see view.ControlsModel#isDevelopMode()
	 */
	@Override
	public boolean isDevelopMode() {

		return config.isDevelopeMode();
	}
}
