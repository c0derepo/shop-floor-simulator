/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package view;

import java.util.OptionalInt;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import shop.Checkout;
import shop.Checkout.State;
import shop.CustomerQueue;

/**
 * CheckOutModelAdapter.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 28 Oct 2018
 *
 */
public class CheckOutModelAdapter implements CheckoutModel {

	/**
	 *
	 */
	private final CustomerQueue customerQueue;

	/**
	 *
	 */
	private final Checkout checkout;

	/**
	 *
	 */
	private int optimalQueueCapcity;

	// ///////////////////////////////////////////////////////////
	// ////////////////// Constructor ////////////////////////////
	// ///////////////////////////////////////////////////////////
	/**
	 *
	 */
	public CheckOutModelAdapter(final Checkout checkout) {

		this.checkout = checkout;
		customerQueue = checkout.getQueue();
		optimalQueueCapcity = 5; //default
	}

	/* (non-Javadoc)
	 * @see view.CheckoutModel#setCheckoutToState(shop.Checkout.State)
	 */
	@Override
	public void setCheckoutToState(final State state) {

		switch (state) {

			case ON: case IDLE: case BUSY:
				checkout.start();
				break;

			case OFF:
				checkout.off();
				break;

			case STOPED:
				checkout.stop();
				break;
	   }
	}

	// ///////////////////////////////////////////////////////////
	// ////////////////// Getters, Setters ///////////////////////
	// ///////////////////////////////////////////////////////////
	/**
	 * @see view.CheckoutModel#isEmpty()
	 */
	@Override
	public boolean isEmpty() {

		return customerQueue.isEmpty();
	}

	/**
	 * @see view.CheckoutModel#isFull()
	 */
	@Override
	public boolean isFull() {

		return customerQueue.isFull();
	}

	/**
	 * @see view.CheckoutModel#getQueueCapcity()
	 */
	@Override
	public int getQueueCapcity() {

		return customerQueue.getQueueCapcity();
	}

	/* (non-Javadoc)
	 * @see view.CheckoutModel#getOptimalCapcity()
	 */
	@Override
	public int getOptimalCapcity() {

		return optimalQueueCapcity;
	}

	/**
	* Set {@link #optimalQueueCapcity}
	*/
	void setOptimalCapcity(final int optimalQueueCapcity) {

		this.optimalQueueCapcity = optimalQueueCapcity;
	}

	/**
	 * @see view.CheckoutModel#getNumOfQueCustomers()
	 */
	@Override
	public SimpleIntegerProperty getNumOfQueCustomers() {

		return customerQueue.numOfQueCustomers();
	}

	/**
	 * @see view.CheckoutModel#getQueueId()
	 */
	@Override
	public int getQueueId() {

		return customerQueue.getQueueId();
	}

	/**
	 * @see view.CheckoutModel#checkOutStateProperty()
	 */
	@Override
	public SimpleObjectProperty<State> checkOutStateProperty() {

		return checkout.getState();
	}

	/**
	 * @see view.CheckoutModel#getCheckOutId()
	 */
	@Override
	public int getCheckOutId() {

		return checkout.getCheckOutId();
	}

	/**
	 * @see view.CheckoutModel#getCustomerId()
	 */
	@Override
	public SimpleObjectProperty<OptionalInt> getCustomerId() {

		return checkout.getCustomerId();
	}

	/* (non-Javadoc)
	 * @see view.CheckoutModel#isShuttingDown()
	 */
	@Override
	public SimpleBooleanProperty isShuttingDown() {

		return checkout.shuttingDownProperty();
	}


	/* (non-Javadoc)
	 * @see view.CheckoutModel#isStopping()
	 */
	@Override
	public SimpleBooleanProperty isStopping() {

		return checkout.stoppingProperty();
	}
}
