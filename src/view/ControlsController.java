/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import main.Controller;
import shop.ShopController.ShopState;

/**
 * ControlsController.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 21 Oct 2018
 *
 */
public class ControlsController implements Controller<ControlsModel>{

	@FXML Button openBtn;
	@FXML ToggleButton closeBtn;
	@FXML Slider slider;
	@FXML CheckBox autoPilot;

	private ControlsModel model;

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	@FXML protected void initialize() {

		slider.valueProperty().addListener((obs, oldValue, newValue) -> {

			updateSliderToolTip();
			updateSimulationSpeed();
		});

		closeBtn.selectedProperty().addListener((obs, oldValue, newValue) -> close(newValue));
		closeBtn.setSelected(false);
	}

	@Override
	public void setModel(final ControlsModel model){

		this.model = model;

		openBtn.setOnAction(e -> model.openCloseDoor());
		closeBtn.setOnAction(e -> model.closeShop());

		slider.setValue(model.getSimultionSpeed());
		slider.setMax(model.isDevelopMode() ? 10 : 5);

		autoPilot.setSelected(model.autoPilotProperty().getValue());
		autoPilot.selectedProperty().addListener((obs, oldValue, newValue) -> {
			model.autoPilotProperty().set(newValue);
		});

		updateGui(model.shopStateProperty().get()); //initial gui state
		model.shopStateProperty().addListener((obs, oldValue, newValue)->{
			updateGui(newValue);
		});
	}

	/**
	 *
	 */
	private void updateSliderToolTip() {

		slider.getTooltip().setText( "Speed: " + (int)slider.getValue());
	}

	private void close(final Boolean selected) {

		openBtn.setDisable(selected);
		slider.setDisable(selected);
		autoPilot.setDisable(selected);
		closeBtn.setText(selected ? "  Closing   " : "Close Shop");
		closeBtn.setDisable(selected);
	}

	/**
	 *
	 */
	private void updateSimulationSpeed() {

		model.setSimultionSpeed(slider.getValue());
	}

	/**
	 *
	 */
	public void updateGui(final ShopState state) {

		switch(state) {

			case OPEN:
				openBtn.setText("Close Door");
				openBtn.setStyle(" -fx-text-fill: red ");
				closeBtn.setDisable(false);
				return ;

			case DOORS_CLOSED:
				openBtn.setText("Open Door");
				openBtn.setStyle(" -fx-text-fill: cyan ");
				return ;

			case CLOSED: default:
				openBtn.setText("Open Shop");
				openBtn.setStyle(" -fx-text-fill: cyan ");
				closeBtn.setSelected(false);
				closeBtn.setDisable(true);
				return ;
		}
	}
}