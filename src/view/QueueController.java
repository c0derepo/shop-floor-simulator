/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package view;

import java.io.IOException;
import java.net.URL;
import java.text.NumberFormat;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import main.Controller;
import main.TypeFromXml;
import progress_bar.ProgressbarFx;
import progress_bar.ProgressbarFxModel;
import progress_bar.ProgressbarModel.Progress;

/**
 * QueueController.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 21 Oct 2018
 *
 */
public class QueueController implements Controller<CheckoutModel>{

	/**
	 *
	 */
	@FXML private Label cashier, customer;
	@FXML private VBox queuePane;

	/**
	 *
	 */
	final NumberFormat formatter = NumberFormat.getIntegerInstance();

	/**
	 *
	 */
	private CheckoutModel model;

	/**
	 *
	 */
	private Timeline flasher;

	/**
	 *
	 */
	private SimpleBooleanProperty isShuttingOrStopping;

	/**
	 *
	 */
	private PseudoClass onStyle, offStyle, busyStyle, stopedStyle, flashingStyle;

	/**
	 *
	 */
	private ProgressBar progressBar;

	/**
	 *
	 */
	private ProgressbarFx queueBar;

	private static final String ON = "on", OFF = "off", BUSY = "busy", STOPED = "stoped", FLASH = "flash";
	private static final double MIN_QUEUE_WIDTH = 150, MIN_QUEUE_HIGHT = 35;

    /**
	 *
	 */
	private PseudoClass lowStyle, mediumStyle, highStyle;
	private static final String LOW  = "low-bar", MEDIUM  = "medium-bar", HIGH = "high-bar";

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	@FXML
	public void initialize(){

		lowStyle = PseudoClass.getPseudoClass(LOW);
		mediumStyle = PseudoClass.getPseudoClass(MEDIUM);
		highStyle = PseudoClass.getPseudoClass(HIGH);

		queueBar = new ProgressbarFx();

		//checkout styles
		onStyle = PseudoClass.getPseudoClass(ON);
		offStyle = PseudoClass.getPseudoClass(OFF);
		busyStyle = PseudoClass.getPseudoClass(BUSY);
		stopedStyle = PseudoClass.getPseudoClass(STOPED);
		flashingStyle = PseudoClass.getPseudoClass(FLASH);

		//flasher for busy state
		flasher = new Timeline(
				new KeyFrame(Duration.seconds(0.5), e -> {
					cashier.pseudoClassStateChanged(flashingStyle, true);
				}),

				new KeyFrame(Duration.seconds(1.0), e -> {
					cashier.pseudoClassStateChanged(flashingStyle, false);
				})
				);
		flasher.setCycleCount(Animation.INDEFINITE);
	}

	@FXML protected void cashierPressed(final MouseEvent event) {

		TypeFromXml<Parent,CheckoutModel> parent = null;
		try {
			final URL url = getClass().getResource("xml/cashier_options.fxml");
			parent = new TypeFromXml<>(url, model, Parent.class);
		} catch (final IOException ex) {
			ex.printStackTrace();
		}

		final Scene scene = new Scene(parent.getNode());
		final Stage stage = new Stage(StageStyle.UNDECORATED);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setScene(scene);
		stage.show();
	}

	@Override
	public void setModel(final CheckoutModel model){

		this.model = model;
		final ProgressbarFxModel barModel = new ProgressbarFxModel
								(model.getNumOfQueCustomers(),model.getQueueCapcity() );
    	barModel.setCssUrl(getClass().getResource("xml/queueProgressBar.css"))
    		    .setKeepLabelHorizontal(true)
    	        .setProgressDir(Progress.UP)
    	 		.setOnProgressChangeDo(d-> styleBar(d));
    	queueBar.setModel(barModel);

    	try {
			queuePane.getChildren().add(queueBar.makeProgressBar());
		} catch (final IOException ex) {
			ex.printStackTrace();
		}

		progressBar = queueBar.getProgressBar();
		progressBar.setMinHeight(MIN_QUEUE_HIGHT);
		progressBar.setMinWidth(MIN_QUEUE_WIDTH);

		final String checkOutId = String.valueOf(model.getCheckOutId());
		cashier.setText("Cashier # " + checkOutId);

		//style cashier as per its status
		styleCashier();
		model.checkOutStateProperty().addListener( (obs,oldValue,newValue)-> {

			styleCashier();
		});

		model.getCustomerId().addListener( (observable,oldValue,newValue)-> {

			final int id = newValue.orElse(-1);
			Platform.runLater(()-> {
				customer.setText( id > 0 ? "Customer "+ id: " ");
			});
		});

		isShuttingOrStopping = new SimpleBooleanProperty(false);
		isShuttingOrStopping.bind(model.isShuttingDown().or(model.isStopping()));
		isShuttingOrStopping.addListener( (obs, oldValue, newValue) ->{
			if(newValue) {
				flasher.play();
			}else {
				flasher.stop();
				cashier.pseudoClassStateChanged(flashingStyle, false);
				styleCashier();//restore style
			}
		});
	}

	/**
	 *@param newValue
	 */
	private void styleCashier() {

		cashier.pseudoClassStateChanged(onStyle, false);
		cashier.pseudoClassStateChanged(offStyle, false);
		cashier.pseudoClassStateChanged(busyStyle, false);
		cashier.pseudoClassStateChanged(stopedStyle, false);

		switch (model.checkOutStateProperty().get()) {

		case ON: case IDLE:
			cashier.pseudoClassStateChanged (onStyle, true);
			break;

		case OFF:
			cashier.pseudoClassStateChanged (offStyle, true);
			break;

		case BUSY:
			cashier.pseudoClassStateChanged(busyStyle, true);
			break;

		case STOPED:
			cashier.pseudoClassStateChanged (stopedStyle, true);
			break;
		}
	}

    private void styleBar(final double progress) {

    	if(lowStyle == null || mediumStyle == null || lowStyle == null)
			return ;

    	progressBar.pseudoClassStateChanged (lowStyle, false);
    	progressBar.pseudoClassStateChanged (mediumStyle, false);
    	progressBar.pseudoClassStateChanged (highStyle, false);

    	if(progress < getLowRangeLimit() ) {
    		progressBar.pseudoClassStateChanged (lowStyle, true);
    	}else if( progress < getMediumRangeLimit()) {
    		progressBar.pseudoClassStateChanged (mediumStyle, true);
    	}else {
    		progressBar.pseudoClassStateChanged (highStyle, true);
    	}
    }

	/**
	 *@return
	 */
	private double getLowRangeLimit() {

		final double optimal = (double)model.getOptimalCapcity()/model.getQueueCapcity() ;
		return 0.8 * optimal;
	}

	/**
	 *@return
	 */
	private double getMediumRangeLimit() {

	    final double optimal = (double)model.getOptimalCapcity()/model.getQueueCapcity() ;
	    return 1.25 * optimal;
	}

}
