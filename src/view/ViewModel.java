/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package view;

/**
 * ViewModel.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 25 Oct 2018
 *
 */
public interface ViewModel extends HeaderModel, FloorModel,
									CheckoutsModel ,ControlsModel{
}
