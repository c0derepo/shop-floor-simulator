/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package view;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.scene.layout.VBox;
import main.NodeFromXml;


/**
 * RootController.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 21 Oct 2018
 *
 */
public class RootController {

	@SuppressWarnings("rawtypes")
	private NodeFromXml headerPane, floorPane, checkoutPane, controlsPane;
	private static final int QUEUE_WIDTH = 110;
	@FXML private VBox root;

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////
	@FXML
	public void initialize(){

		try {
			headerPane   = new NodeFromXml<HeaderModel>(getClass().getResource("xml/header.fxml"));
			floorPane    = new NodeFromXml<FloorModel>(getClass().getResource("xml/floor.fxml"));
			checkoutPane = new NodeFromXml<CheckoutsModel>(getClass().getResource("xml/checkouts.fxml"));
			controlsPane = new NodeFromXml<ControlsModel>(getClass().getResource("xml/controls.fxml"));
		} catch (final IOException ex) {
			ex.printStackTrace();
		}

		root.getChildren().addAll(headerPane.getNode(), floorPane.getNode(),
								checkoutPane.getNode(), controlsPane.getNode());
	}

	@SuppressWarnings("unchecked")
	void setModel(final ViewModel model){
		//set shop view width
		root.setPrefWidth(model.getNumbrOfQueues()* QUEUE_WIDTH);

		headerPane.setModel(model);
		floorPane.setModel(model);
		checkoutPane.setModel(model);
		controlsPane.setModel(model);
	}
}
