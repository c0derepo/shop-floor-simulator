/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import main.Controller;
import shop.Checkout.State;

/**
 *
 * CashierOptionsController.java
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 4 Nov 2018
 *
 */
public class CashierOptionsController implements Controller<CheckoutModel>{

	@FXML Button cancelBtn, updateBtn;
	@FXML Label header, info;
	@FXML ToggleGroup buttonsGroup;
	@FXML RadioButton onBtn, stopBtn, offBtn;
	@FXML HBox infoPane;

	/**
	 *
	 */
	private CheckoutModel model;

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	@FXML
	public void initialize(){
		updateBtn.setOnAction(e -> update());
		cancelBtn.setOnAction(e -> cancel());
	}

	/**
	 *Update model with requested state
	 */
	private void update() {

		final String selected = ((RadioButton)buttonsGroup.getSelectedToggle()).getText().toLowerCase();

		State state = State.ON;
		switch(selected) {
		case "off":
			state = State.OFF;
			break;
		case "stop":
			state = State.STOPED;
			break;
		}

		model.setCheckoutToState(state);
		cancel();
	}

	/**
	 *
	 */
	private void cancel() {

		((Stage) updateBtn.getScene().getWindow()).close();
	}

	/**
	 *@param state
	 */
	private void updateButtons(final State state) {

		if(model.isStopping().get()) {
			stopBtn.setSelected(true);
			return;
		}

		if(model.isShuttingDown().get()) {
			offBtn.setSelected(true);
			stopBtn.setDisable(true);
			return;
		}

		switch (state) {

		case ON: case IDLE: case BUSY:
			onBtn.setSelected(true);
			break;

		case OFF:
			offBtn.setSelected(true);
			stopBtn.setDisable(true);
			break;

		case STOPED:
			stopBtn.setSelected(true);
			break;
		}
	}

	// ///////////////////////////////////////////////////////////
	// ////////////////// Getters, Setters ///////////////////////
	// ///////////////////////////////////////////////////////////


	/* (non-Javadoc)
	 * @see view.ControlMaster#setModel(java.lang.Object)
	 */
	@Override
	public void setModel(final CheckoutModel model) {

		this.model = model;

		header.setText("Cashier #"+ model.getCheckOutId());
		updateButtons(model.checkOutStateProperty().get());

		String text ="";

		if(model.isStopping().get()) {
			text =   "Checkout is stopping" ;
		}else if(model.isShuttingDown().get()) {
			text = "Checkout is shutting down";
		}else {
			infoPane.setManaged(false); //hide info pane
		}
		info.setText(text);
	}
}

