/*
 * To change this template, choose Tools | Template
 * https://bitbucket.org/c0derepo/
 */
package main;

/**
 * Represents app informations and different app running modes
 *
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 24 Oct 2015
 *
 */
public final class About {

	/**
	 * Represents different app running modes
	 */
	public enum Mode {

		/**
		 * Regular user mode
		 */
		USE    (0) ,

		/**
		 * User debugging mode
		 */
		DEBUG  (1),

		/**
		 * Development mode
		 */
		DEV    (2);

		public int value;

		Mode(final int value){
			this.value = value;
		}

		public static Mode getModeByValue(final int value){

			for (final Mode m: Mode.values()) {
				if( m.value == value)
					return m;
			}
			return null;
		}

		public boolean isBigger( final Mode s2){

			return compareTo(s2) > 0;
		}
	}

	/**
	 * App run mode
	 */
	private static Mode mode = null;

	/**
	 * Short displayName of this application
	 */
	private final static String APP_SHORT_NAME = "Shop Sim";

	/**
	 * Full displayName of this application
	 */
	private final static String APP_NAME = "Shop Simulation";

	/**
	 * Version of this application
	 */
	private final static float VER = (float) 1.00;

	/**
	 *
	 */
	private final static String AUTHOR = "c0der \u00a9 4c.app.testing@gmail.com";

	// prevent creating an instance
	private  About() { throw new AssertionError(); }

	// ///////////////////////////////////////////////////////////
	// ////////////////// Getters, Setters ///////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 * Application displayName.
	 */
	public static String getAppName() {
		return APP_NAME;
	}

	/**
	 * Application short displayName.
	 */
	public static String getAppShortName() {
		return APP_SHORT_NAME;
	}

	/**
	 * Application version.
	 */
	public static float getVer() {
		return VER;
	}

	/**
	 * Application displayName including it version
	 * in the format of displayName-version.
	 */
	public static String getAppFullName() {
		return APP_SHORT_NAME+"-"+VER;
	}

	/**
	 * Application author.
	 */
	public static String getAuthor() {
		return AUTHOR;
	}

	/**
	 * Get {@link #mode}
	 */
	public static Mode getMode() {

		return mode;
	}

	/**
	 * Set {@link #mode}
	 */
	public static void setMode(final Mode mode) {

		About.mode = mode == null ?  Mode.USE : mode;
	}

	/**
	 * Flag if application is in {@link Mode#DEBUG}
	 */
	public static boolean isDebug() {

		return getMode() == Mode.DEBUG;
	}

	/**
	 * Flag if application is in {@link Mode#DEV}
	 */
	public static boolean isDevelope() {

		return getMode() == Mode.DEV;
	}
}
