/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package main;

import javafx.application.Application;
import view.MainView;

/**
 * Main.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 26 Oct 2018
 *
 */
public class Main {

	/**
	 *
	 */
	public static void main(String[] args) {

		Application.launch(MainView.class);
	}
}
