/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package main;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 *
 * ThreadsSupplier.java
 *
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 31 Oct 2018
 *
 */
public final class ThreadsSupplier {

	/**
	 *
	 */
	private static final int DEFAULT_NUMBER_OF_THREADS = 16;

	/**
	 * Number of threads in {@link #service}<br/>
	 * Can be set only before {@link #service} is created<br/>
	 */
	private static int numberOfThreads = DEFAULT_NUMBER_OF_THREADS;

	/**
	 *
	 */
	private static ScheduledExecutorService service;

	//prevent creating an instance
	private ThreadsSupplier() {throw new AssertionError();}


	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////
	/**
	 *
	 */
	public static ScheduledExecutorService getExecutor() {

		if(service == null) {
			service = Executors.
					newScheduledThreadPool(getNumberOfThreads());
		}

		return service;
	}

	/**
	 *
	 */
	public static ScheduledExecutorService getSingleThreadExecutor() {

		return Executors.newSingleThreadScheduledExecutor();
	}

	// ///////////////////////////////////////////////////////////
	// ////////////////// Getters, Setters ///////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	* Get {@link #numberOfThreads}
	*/
	public static int getNumberOfThreads() {

		return numberOfThreads;
	}

	/**
	* Set {@link #numberOfThreads}
	*/
	public static void setNumberOfThreads(final int numberOfThreads) {

		ThreadsSupplier.numberOfThreads = numberOfThreads;
	}
}
