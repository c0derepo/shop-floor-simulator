/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package main;

import java.io.File;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;

/**
 *
 *Utils.java
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 26 Nov 2018
 *
 */
public final class Utils {

    // prevent creating an instance
   	private  Utils() { throw new AssertionError(); }

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////
	/**
	 * <pre>
	 * Checks if a string is a valid path.
	 * Null safe.
	 *
	 * Calling examples:
	 *    isValidPath("c:/test");      //returns true
	 *    isValidPath("c:/te:t");      //returns false
	 *    isValidPath("c:/te?t");      //returns false
	 *    isValidPath("c/te*t");       //returns false
	 *    isValidPath("good.txt");     //returns true
	 *    isValidPath("not|good.txt"); //returns false
	 *    isValidPath("not:good.txt"); //returns false
	 * </pre>
	 */
	public static boolean isValidPath(final String path) {
	    try {
	        Paths.get(path);
	    } catch (InvalidPathException | NullPointerException ex) {
	        return false;
	    }
	    return true;
	}

	/**
	 * Check if test folder exists (to indicate development mode)
	 */
	public static boolean isTestFolderExist() {

		final String path = "test";
		if(! isValidPath(path))	return false;

		return new File(path).exists();
	}

	/**
	 * <pre>
	 * Checks if dirPath is a folder.
	 *
	 * @param dirPath
	 * <br>   Null safe.
	 *<pre>
	 */
	public static boolean isDirectory(final String dirPath) {

		if(! isValidPath(dirPath))	return false;

		return  new File(dirPath).isDirectory();
	}
}
