package main;

import javafx.beans.property.SimpleBooleanProperty;

/**
 * Config.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 25 Oct 2018
 *
 */
public final class Config {

	/**
	 *
	 */
	private static final int DEFAULT_NUMBER_OF_CHCKOUTS = 6;

	/**
	 *
	 */
	private static final int DEFAULT_MIN_OF_CHCKOUTS = 2;

	/**
	 *
	 */
	private static final int DEFAULT_SHOP_CAPACITY = 180;

	/**
	 *
	 */
	private static final int DEFAULT_QUEUE_CAPACITY = 10;

	/**
	 * The desired number of customers in a queue default
	 */
	private static final int DEFAULT_OPTIMAL_QUEUE = 5;

	/**
	 *
	 */
	private static final String DEFAULT_SHOP_NAME = "Super Shop";

	/**
	 *
	 */
	private static final int DEFAULT_MAX_CUSTOMERS_RATE = 20;

	/**
	 * Number of queues load /balance checks per hour
	 */
	private static final int DEFAULT_QUEUES_BALANCE_RATE = 60;

	/**
	 * Number of checkouts load checks per hour
	 */
	private static final int DEFAULT_CHECKOUTS_LOAD_CHECK_RATE = 12;

	/**
	 * Min number of customers on floor, at start, before
	 * checkouts become active
	 */
	private static final int DEFAULT_MIN_FLOOR_CUSTOMERS = 15;

	/**
	 * Default factor for increasing / decreasing simulation speed
	 */
	private static final int DEFAULT_SPEED_FACTOR = 1;

	/**
	 * Use to increase / decrease entry of new customer <br/>
	 * 0.8 means lower speed, higher interval between customers
	 */
	private static final double LOAD_FLOOR_ACCELERATION_FACTOR = 0.95;

	/**
	 * Use to increase / decrease queue load speed. <br/>
	 * 0.8 means lower speed,  higher interval between customers
	 */
	private static final double LOAD_QUEUES_ACCELERATION_FACTOR = 0.95;

	/**
	 * Use to increase / decrease checkout speed. 1 means equal to {@link #speedFactor} <br/>
	 * 1.1 means higher speed
	 */
	private static final double CHECKOUT_ACCELERATION_FACTOR = 1.25;

	/**
	 * Default set to {@link #DEFAULT_NUMBER_OF_CHCKOUTS}
	 */
	private final int maxNumberOfCheckouts;

	/**
	 * Default to half {@link #DEFAULT_MIN_OF_CHCKOUTS}<br/>
	 * Valid values are <= {@link #maxNumberOfCheckouts}
	 */
	private int minNumberOfCheckouts;

	/**
	 * Max number of customers in shop<br/>
	 * Default set to {@link #DEFAULT_SHOP_CAPACITY}
	 */
	private int shopCapacity;

	/**
	 * Min number of customers on floor, at start, before
	 * checkouts become active<br/>
	 * Default set to {@link #DEFAULT_MIN_FLOOR_CUSTOMERS}<br/>
	 * Valid values are <= {@link #shopCapacity}
	 */
	private int minFloorCustomers;

	/**
	 * Max number of customers in a queue<br/>
	 * Default set to {@link #DEFAULT_QUEUE_CAPACITY}
	 */
	private int queueCapacity;

	/**
	 * The desired number of customers in a queue
	 * Default set to {@link #DEFAULT_OPTIMAL_QUEUE}<br/>
	 * Valid values are <= {@link #queueCapacity}
	 */
	private int optimalQueueSize;

	/**
	 * Default set to {@link #DEFAULT_SHOP_NAME}
	 */
	private String shopName;

	/**
	 * <pre>
	 * Max rate of customers entering the shop: times per minute <br/>
	 * Default set to {@link #DEFAULT_MAX_CUSTOMERS_RATE}
	 * Valid values are > 0
	 * Returned value is effected by {@link #speedFactor}
	 * <pre>
	 */
	private int maxCustomersPerMin;

	/**
	 * <pre>
	 * Rate of invoking queues load / balance checks: times per hour
	 * Default set to {@link #DEFAULT_QUEUES_BALANCE_RATE}
	 * Returned value is effected by {@link #speedFactor}
	 * </pre>
	 */
	private final int queuesLoadChecksPerHour;

	/**
	 * <pre>
	 * Rate of invoking checkouts load checks: times per hour
	 * Default set to {@link #DEFAULT_CHECKOUTS_LOAD_CHECK_RATE}
	 * Returned value is effected by {@link #speedFactor}
	 * </pre>
	 */
	private final int checkoutsLoadChecksPerHour;

	/**
	 * <pre>
	 * Factor to increase / decrease simulation speed
	 * Increasing the factor increases simulation speed
	 * Default set to {@link #DEFAULT_SPEED_FACTOR}
	 * Valid values are > 0
	 * <pre/>
	 *
	 */
	private double speedFactor;

	/**
	 * Flags that the shop was just opened<br/>
	 * Default is set to false;
	 */
	private boolean isJustOpened ;

	/**
	 * Encapsulates flag to set auto open - close checkouts automatically
	 */
	private final SimpleBooleanProperty autoPilotProperty;

	// ///////////////////////////////////////////////////////////
	// ////////////////// Constructor ////////////////////////////
	// ///////////////////////////////////////////////////////////
	/**
	 *
	 */
	public Config() {

		maxNumberOfCheckouts = DEFAULT_NUMBER_OF_CHCKOUTS;
		minNumberOfCheckouts = DEFAULT_MIN_OF_CHCKOUTS;
		shopCapacity = DEFAULT_SHOP_CAPACITY;
		queueCapacity = DEFAULT_QUEUE_CAPACITY;
		optimalQueueSize = DEFAULT_OPTIMAL_QUEUE;
		shopName = DEFAULT_SHOP_NAME;
		maxCustomersPerMin = DEFAULT_MAX_CUSTOMERS_RATE;
		minFloorCustomers = DEFAULT_MIN_FLOOR_CUSTOMERS;
		queuesLoadChecksPerHour = DEFAULT_QUEUES_BALANCE_RATE;
		checkoutsLoadChecksPerHour = DEFAULT_CHECKOUTS_LOAD_CHECK_RATE;
		speedFactor = DEFAULT_SPEED_FACTOR;
		isJustOpened = false;
		autoPilotProperty = new SimpleBooleanProperty(true);
	}

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 *
	 */
	public double getCheckoutSpeedFactor() {

		return speedFactor * CHECKOUT_ACCELERATION_FACTOR;
	}

	// ///////////////////////////////////////////////////////////
	// ////////////////// Getters, Setters ///////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	* Get {@link #shopCapacity}
	*/
	public int getShopCapacity() {
		return shopCapacity;
	}

	/**
	* Set {@link #shopCapacity}
	*/
	public void setShopCapacity(final int shopCapacity) {
		this.shopCapacity = shopCapacity;
	}

	/**
	* Get {@link #queueCapacity}
	*/
	public int getQueueCapacity() {

		return queueCapacity;
	}

	/**
	* Set {@link #queueCapacity}
	*/
	public void setQueueCapacity(final int queueCapacity) {

		this.queueCapacity = queueCapacity;
	}

	/**
	* Get {@link #optimalQueueSize}
	*/
	public int getOptimalQueueSize() {
		return optimalQueueSize;
	}

	/**
	* Set {@link #optimalQueueSize}
	*/
	public void setOptimalQueueSize(final int optimalQueueSize) {
		this.optimalQueueSize = optimalQueueSize <= getQueueCapacity() ?
											optimalQueueSize : optimalQueueSize;
	}

	/**
	*
	*/
	public int getSBufferQueueCapacity() {

		return getQueueCapacity() * 3;
	}

	/**
	* Get {@link #minFloorCustomers}
	*/
	public int getMinFloorCustomers() {
		return minFloorCustomers;
	}

	/**
	* Set {@link #minFloorCustomers}
	*/
	public void setMinFloorCustomers(final int minFloorCustomers) {

		this.minFloorCustomers = minFloorCustomers < getShopCapacity() ?
				minFloorCustomers : this.minFloorCustomers;
	}

	/**
	* Get {@link #maxNumberOfCheckouts}
	*/
	public int getMaxNumberOfCheckouts() {

		return maxNumberOfCheckouts;
	}

	/**
	* Get {@link #minNumberOfCheckouts}
	*/
	public int getMinNumberOfCheckouts() {

		return minNumberOfCheckouts;
	}

	/**
	* Set {@link #minNumberOfCheckouts}
	*/
	void setMinNumberOfCheckouts(final int minNumberOfCheckouts) {

		this.minNumberOfCheckouts = minNumberOfCheckouts > getMaxNumberOfCheckouts() ?
							this.minNumberOfCheckouts : minNumberOfCheckouts;
	}

	/**
	* Get {@link #shopName}
	*/
	public String getShopName() {
		return shopName;
	}

	/**
	* Set {@link #shopName}
	*/
	public void setShopName(final String shopName) {
		this.shopName = shopName;
	}

	/**
	* Get {@link #maxCustomersPerMin}
	*/
	public int getMaxCustomersPerMin() {

		return (int) (maxCustomersPerMin * getSpeedFactor());
	}

	/**
	* Set {@link #maxCustomersPerMin}
	*/
	public void setMaxCustomersPerMin(final int maxCustomersPerMin) {

		this.maxCustomersPerMin = maxCustomersPerMin > 0 ? maxCustomersPerMin : this.maxCustomersPerMin;
	}

	/**
	* Get {@link #queuesLoadChecksPerHour}
	*/
	public int getQueuesLoadChecksPerHour() {

		return (int) (queuesLoadChecksPerHour * getSpeedFactor());
	}


	/**
	* Get {@link #checkoutsLoadChecksPerHour}
	*/
	public int getCheckoutsLoadChecksPerHour() {

		return (int) (checkoutsLoadChecksPerHour * getSpeedFactor());
	}

	/**
	* Get {@link #speedFactor}
	*/
	public double getSpeedFactor() {
		return speedFactor;
	}

	/**
	* Set {@link #speedFactor}
	*/
	public void setSpeedFactor(final double speedFactor) {

		this.speedFactor = speedFactor > 0 ? speedFactor : this.speedFactor;
	}

	/**
	* Get {@link #isJustOpened}
	*/
	public synchronized boolean isJustOpened() {
		return isJustOpened;
	}

	/**
	* Set {@link #isJustOpened}
	*/
	public synchronized void setJustOpened(final boolean isJustOpened) {
		this.isJustOpened = isJustOpened;
	}

	/**
	* Get {@link #autoPilotProperty}
	*/
	public SimpleBooleanProperty autoPilotProperty() {
		return autoPilotProperty;
	}

	/**
	* Set {@link #autoPilotProperty} value
	*/
	public void setAutoPilot(final boolean isAutoPilot) {

		autoPilotProperty.set(isAutoPilot);
	}

	/**
	* Get {@link #autoPilotProperty}
	*/
	public SimpleBooleanProperty getAutoPilotProperty() {

		return autoPilotProperty;
	}

	/**
	 *
	 */
	public boolean isDevelopeMode(){

		return About.isDevelope();
	}

	/**
	* Get {@link #LOAD_QUEUES_ACCELERATION_FACTOR}
	*/
	public double getLoadQueuesAccelerationFactor() {

		return LOAD_QUEUES_ACCELERATION_FACTOR;
	}

	/**
	* Get {@link #LOAD_FLOOR_ACCELERATION_FACTOR}
	*/
	public double getLoadFloorAccelerationFactor() {

		return LOAD_FLOOR_ACCELERATION_FACTOR;
	}
}
