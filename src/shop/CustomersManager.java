/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package shop;

import java.util.concurrent.TimeUnit;

import main.Config;
import main.ThreadsSupplier;

/**
 * Warps a {@link CustomerFactory}
 *
 * CustomersManager.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 20 Oct 2018
 *
 */
public class CustomersManager {

	/**
	 * Produces new customers
	 */
	private final CustomerFactory factory;

	/**
	 *
	 */
	private static Config config;

	// ///////////////////////////////////////////////////////////
	// ////////////////// Constructor ////////////////////////////
	// ///////////////////////////////////////////////////////////
	/**
	 *
	 */
	CustomersManager(final FloorCustomers customers, final Config config) {

		CustomersManager.config = config;
		factory = new CustomerFactory(customers, config);
	}

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////
	/**
	 *
	 */
	void start() {

		factory.start();
	}

	/**
	 *
	 */
	void stop() {
		factory.stop();
	}

	/**
	 *
	 *@return
	 */
	public static long getRandomWaitBetweenCustomers() {

		return getRandomWaitBetweenCustomers(1);
	}

	/**
	 * Random wait in milliseconds<br/>
	 * @param accelerationFactor
	 * Valid values > 0
	 */
	public static long getRandomWaitBetweenCustomers(double accelerationFactor) {

		accelerationFactor = accelerationFactor > 0 ? accelerationFactor : 1 ;
		double rate = randomCustomersPerMinute() * accelerationFactor;

		rate = rate > 0 ? 60000 / rate : 0 ; //1 min = 60 sec = 60,000 milli

		return (long) rate;
	}

	/**
	 * Random rate between 0 and {@link Config#getMaxCustomersPerMin()}
	 */
	public static int randomCustomersPerMinute() {

		return Random.upTo(config.getMaxCustomersPerMin());
	}
}

/**
 * Periodically add {@link Customer} to {@link FloorCustomers}
 *
 * CustomerFactory .java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 20 Oct 2018
 *
 */
class CustomerFactory {

	/**
	 * Increase customer adding rate at start
	 */
	private static final int RATE_FACTOR_AT_START = 5;

	/**
	 *
	 */
	private final FloorCustomers customers;

	/**
	 *
	 */
	private boolean isTerminate;

	/**
	 *
	 */
	private boolean isRunning;

	/**
	 *
	 */
	private Runnable addCustomer;

	/**
	 *
	 */
	private final Config config;

	// ///////////////////////////////////////////////////////////
	// ////////////////// Constructor ////////////////////////////
	// ///////////////////////////////////////////////////////////
	/**
	 *
	 */
	public CustomerFactory(final FloorCustomers customers, final Config config) {
		this.customers= customers;
		this.config = config;
		isTerminate = false; isRunning = false;
	}

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 * Periodically add {@link Customer} to {@link FloorCustomers}
	 */
	void start() {

		isTerminate = false;
		if(isRunning()) return;
		setRunning(true);

		addCustomer = ()-> {

			Thread.currentThread().setName("Add customer");
			//add
			customers.addCustomer(new Customer(config));

			//handle just-opened flag
			if(config.isJustOpened() &&
					customers.numOfFloorCustomers().get() > config.getMinFloorCustomers()) {
				config.setJustOpened(false);
			}

			//schedule next add
			if(! isTerminate) {
				final long wait = config.isJustOpened() ?  	CustomersManager.
						getRandomWaitBetweenCustomers(RATE_FACTOR_AT_START) : //accelerate at start
							 CustomersManager.getRandomWaitBetweenCustomers(config.getLoadFloorAccelerationFactor());
				ThreadsSupplier.getExecutor().schedule(addCustomer,	wait , TimeUnit.MILLISECONDS);
			}else {
				setRunning(false);
			}
		};

		ThreadsSupplier.getExecutor().execute(addCustomer);
	}

	/**
	 *Stop adding {@link Customer} to {@link FloorCustomers}
	 */
	void stop() {
		isTerminate = true;
	}

	// ///////////////////////////////////////////////////////////
	// ////////////////// Getters, Setters ///////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	* Get {@link #isRunning}
	*/
	private synchronized boolean isRunning() {
		return isRunning;
	}

	/**
	* Set {@link #isRunning}
	*/
	private synchronized void setRunning(final boolean isRunning) {
		this.isRunning = isRunning;
	}
}
