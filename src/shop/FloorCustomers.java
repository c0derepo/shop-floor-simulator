/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package shop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javafx.beans.property.SimpleIntegerProperty;
import main.Config;

/**
 * Represents floor customers
 *
 * Customers.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 19 Oct 2018
 *
 */
public class FloorCustomers {

	/**
	 * Represents floor customers
	 */
	private final Set<Customer> customers;

	/**
	 * Represents shop floor customers
	 */
	private final SimpleIntegerProperty numOfFloorCustomers;

	/**
	 * Represents accumulated number of customers
	 */
	private final SimpleIntegerProperty numOfAccumCustomers;

	/**
	 *
	 */
	private final Config config;

	// ///////////////////////////////////////////////////////////
	// ////////////////// Constructor ////////////////////////////
	// ///////////////////////////////////////////////////////////
	/**
	 *
	 */
	protected FloorCustomers( final Config config) {

		this.config = config;
		customers = Collections.synchronizedSet(new HashSet<>());
		numOfFloorCustomers = new SimpleIntegerProperty(0);
		numOfAccumCustomers = new SimpleIntegerProperty(0);
	}

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 *
	 */
	boolean addCustomer(final Customer customer) {

		if(getCustomers().size() >= config.getShopCapacity())
			//log to console customer add blocked
			//System.out.print(".>|.");
			return false;

		final boolean added = customers.add(customer);

		if(added) {
			numOfFloorCustomers().set(customers.size());
			incrementAccumulatedNumOfCustomers();
			//log to console customer added
			//System.out.print(".>.");
		}

		return added;
	}

	/**
	 *
	 */
	boolean removeCustomer(final Customer customer) {

		final boolean removed = customers.remove(customer);
		if(removed) { numOfFloorCustomers().set(customers.size());}

		return removed;
	}

	/**
	 * Returns a random customer without removing it from
	 * {@link #customers}
	 */
	Customer getRandomCustomer(){

		final int numOfCustomersOnFloor = numOfFloorCustomers().get();
		if(numOfCustomersOnFloor <= 0)	return null;

		final int number = Random.upTo(numOfCustomersOnFloor - 1);
		final Customer customer = new ArrayList<>(getCustomers()).get(number);

		return customer;
	}

	// ///////////////////////////////////////////////////////////
	// ////////////////// Getters, Setters ///////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 * Get {@link #customers}
	 */
	private Set<Customer> getCustomers() {
		return customers;
	}

	/**
	 * Get {@link #numOfFloorCustomers}
	 */
	public SimpleIntegerProperty numOfFloorCustomers() {

		return numOfFloorCustomers;
	}

	private void incrementAccumulatedNumOfCustomers() {

		accumulatedNumOfCustomers().set(accumulatedNumOfCustomers().get() +1);
	}

	/**
	* Get {@link #numOfAccumCustomers}
	*/
	public SimpleIntegerProperty accumulatedNumOfCustomers() {

		return numOfAccumCustomers;
	}
}
