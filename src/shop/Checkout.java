package shop;

import java.util.OptionalInt;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import main.ThreadsSupplier;

public class Checkout {

	/**
	 * Checkout states
	 */
	public enum State{
		/**
		 *
		 */
		ON,
		/**
		 *
		 */
		OFF,
		/**
		 *
		 */
		IDLE,
		/**
		 *
		 */
		BUSY,
		/**
		 *
		 */
		STOPED;

		/* (non-Javadoc)
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {

			return name();
		}
	}

	private static final String SPACE = " ";

	/**
	 *
	 */
	private final SimpleObjectProperty<State> state;

	/**
	 *
	 */
	private final SimpleObjectProperty<OptionalInt> customerIdProperty;

	/**
	 * Is checkout accepting new customers<br/>
	 * Check is not active when state is {@link State#STOPED} or {@link State#OFF} and when {@link #isShuttingDown}
	 * or {@link #isStopping} is true
	 */
	private volatile SimpleBooleanProperty isActive;

	/**
	 * Flag indicating if checkout is in stopping process (in a process to change
	 * to {@link State#STOPED})
	 */
	private final SimpleBooleanProperty isStopping;

	/**
	 * Flag indicating if checkout is in shutting-down process (in a process to change
	 * to {@link State#OFF})
	 */
	private final SimpleBooleanProperty isShuttingDown;

	/**
	 *
	 */
	private boolean isSilentChange;

	/**
	 * Flag indicates if {@link #process()} is running
	 */
	private boolean isProcessing;

	/**
	 *
	 */
	private final CustomerQueue queue;

	/**
	 *
	 */
	private ChangeListener<State> listener;

	/**
	 * Unique id
	 */
	private final int checkOutId;

	/**
	 * Is checkout busy (processing a customer);
	 */
	private final SimpleBooleanProperty isBusyProperty;

	/**
	 *
	 */
	private static int uid = 0;

	// ///////////////////////////////////////////////////////////
	// ////////////////// Constructor ////////////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 *
	 */
	Checkout(final CustomerQueue queue) {

		this.queue = queue;
		checkOutId = uid++;

		state = new SimpleObjectProperty<>(State.OFF);

		isStopping = new SimpleBooleanProperty(false);
		isShuttingDown =  new SimpleBooleanProperty(false);
		isActive = new SimpleBooleanProperty(false);

		customerIdProperty = new SimpleObjectProperty<>(OptionalInt.empty());
		isBusyProperty = new SimpleBooleanProperty(false);
		customerIdProperty.addListener( (obs, oldValue, newValue)->{
			isBusyProperty.set(newValue.isPresent());
		});

		isSilentChange = false;
		isProcessing = false;

		listenToStateChanges();
	}

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 *
	 */
	public void start() {

		//check if not already on
		if(isActive().get())  return;
		queue.start(); //change queue state before changing state which triggers balance

		//when checkout changed from off or stop to on it may have a listener
		if(listener != null) {
			state.removeListener(listener);
		}
		//when checkout changed from off or stop to on it may be busy
		final State state = getState().get() == State.BUSY ? State.BUSY : State.ON ;

		//set state, isStopping and isSutting
		SetState(state, false, false);
		process();
	}

	/**
	 * <pre>
	 * Stop queue from accepting more customers and turn off when queue is empty.
	 * While in stopping process {@link #isStoppingProperty().get()} returns true.
	 * At the end of stopping process state changes to {@link State#STOPED}
	 * <pre/>
	 */
	public void stop() {

		//check if not already stopped or stopping
		if( state.get() == State.STOPED ||  stoppingProperty().get())
			return;

		queue.stop(); //change queue state before changing state which triggers balance

		//handle switch from OFF to STOPED :
		//queue balance will not be triggered when changing from one inactive state (OFF)
		//to another (STOPED)
		//switch momentarily to trigger change in isActive which triggers balance
		if(! isActive().get() ) {
			isActive.set(true);
		}

		//set isStopping and isSutting
		SetState(state.get(), true, false);

		if( state.get() != State.BUSY && queue.isEmpty()) {

			//set state, isStopping and isSutting
			SetState(State.STOPED, false, false);
			return;
		}

		//else add a listener and wait for
		addTempListener( (obs, oldV, newV)->{
			if( newV != State.BUSY && queue.isEmpty()) {
				state.removeListener(listener);
				//set state, isStopping
				SetState(State.STOPED, false, false);
			    }
		   }
	  );
	}

	/**
	 * <pre>
	 * Turn checkout off, when it becomes not busy (finish processing if busy)
	 * Queue customers are ignored
	 * While in shutting-down process {@link # isShuttingDownProperty().get()} returns true.
	 * At the end of stopping process state changes to {@link State#OFF}
	 * </pre>
	 */
	public void off() {

		//check if not already off or shutting-down
		if( state.get() == State.OFF ||   shuttingDownProperty().get() )
			return;

		queue.stop(); //change queue state before changing state which triggers balance

		//handle switch from STOPPED to OFF :
		//queue balance will not be triggered when changing from one inactive state (STOPED)
		//to another (OFF)
		//switch momentarily to trigger change in isActive which triggers balance
		if(! isActive().get() ) {
			isActive.set(true);
		}

		//set isStopping and isSutting
		SetState(state.get(), false, true);

		if( state.get() != State.BUSY ) {
			//set state isStopping and isSutting
			SetState(State.OFF, false, false);
			return;
		}

		//else add a listener and wait for
		addTempListener( (obs, oldV, newV)->{
			if( newV != State.BUSY ) {
				state.removeListener(listener);
				//set state and isSutting
				SetState(State.OFF, false, false);
			}
		});
	}

	private void addTempListener(final ChangeListener<State> addListener) {

		if(listener != null) {state.removeListener(listener);}
		listener = addListener;
		state.addListener(listener);
	}

	/**
	 *
	 */
	void process() {

		if(isProcessing()) return;  //if process running, skip
		setProcessing(true);

		ThreadsSupplier.getExecutor().execute(
				() ->{
					Thread.currentThread().setName("Checkout "+ getCheckOutId()+" "+ "queue polling ");
					while(true) {

						//stop condition
						if( state.get()== State.STOPED || state.get()== State.OFF
																	||  shuttingDownProperty().get() ) {
							break;
						}

						//stop condition
						if( stoppingProperty().get() && queue.isEmpty()) {
							break;
						}

						process(queue.waitForNext());
					}

					setProcessing(false);
				}
		  );
	}

	/**
	 *
	 */
	private void process(final Customer customer) {

		customerIdProperty.set(OptionalInt.of(customer.getCustomerId()));

		final Runnable wharpRun = ()->{
			Thread.currentThread().setName("Checkout "+ getCheckOutId());
			customer.run();
		};

		state.set(State.BUSY);

		//wait for thread to finish
		try {

			final Future<?> e = ThreadsSupplier.getExecutor().submit(wharpRun);
			e.get();

		} catch (InterruptedException | ExecutionException ex) {
			ex.printStackTrace();
		}finally {
			state.set(State.IDLE);
		}

		customerIdProperty.set(OptionalInt.empty());
	}

	/**
	 * Is checkout off
	 */
	boolean isOff() {

		return state.get() == State.OFF;
	}

	/**
	 * Is checkout stopped
	 */
	boolean isStopped() {

		return state.get() == State.STOPED;
	}

	/**
	 * Is checkout busy
	 */
	SimpleBooleanProperty isBusyProperty() {

		return isBusyProperty;
	}


	/**
	 * Perform several state changes, without firing multiple events
	 */
	private void SetState(final State state, final boolean isStoping, final boolean isShuttingDown) {

		setSilentChange(true); //change without invoking updateIsActive()
		this.state.set(state);
		isStopping.set(isStoping);
		this.isShuttingDown.set(isShuttingDown);
		setSilentChange(false);

		updateIsActive();
	}

	/**
	 *
	 */
	private void listenToStateChanges() {

		state.addListener( (obs, oldValue, newValue) ->{
			updateIsActive();
		});

		isShuttingDown.addListener( (obs, oldValue, newValue) ->{
			updateIsActive();
		});

		isStopping.addListener( (obs, oldValue, newValue) ->{
			updateIsActive();
		});
	}

	/**
	 * Update {@link #isActive} value
	 */
	private void updateIsActive() {

		if(isSilentChange()) return;

		if(state.get() == State.OFF || state.get() == State.STOPED ||
				shuttingDownProperty().get() || stoppingProperty().get()) {
			isActive.set(false);
		} else {
			isActive.set(true);
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		final StringBuilder sb = new StringBuilder("Checkout ");
		sb.append(getCheckOutId()).
		append(SPACE).append(getState().get());
		if( shuttingDownProperty().get()) {
			sb.append(" shutting down");
		} else if ( stoppingProperty().get()) {
			sb.append(" stopping");
		}

		return sb.toString();
	}

	// ///////////////////////////////////////////////////////////
	// ////////////////// Getters, Setters ///////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 * Get {@link #state}
	 */
	public SimpleObjectProperty<State> getState() {

		return state;
	}

	/**
	 * Get {@link #queue}
	 */
	public CustomerQueue getQueue() {
		return queue;
	}

	/**
	 * Get {@link #checkOutId}
	 */
	public int getCheckOutId() {
		return checkOutId;
	}

	/**
	 * Get {@link #customerIdProperty}
	 */
	public SimpleObjectProperty<OptionalInt> getCustomerId() {
		return customerIdProperty;
	}

	/**
	 * Get {@link #isActive}
	 */
	SimpleBooleanProperty isActive() {
		return isActive;
	}

	/**
	 * Get {@link #isStopping}
	 */
	public SimpleBooleanProperty stoppingProperty() {
		return isStopping;
	}

	/**
	 * Get {@link #isShuttingDown}
	 */
	public SimpleBooleanProperty shuttingDownProperty() {

		return isShuttingDown;
	}

	/**
	 * Get {@link #isSilentChange}
	 */
	private boolean isSilentChange() {
		return isSilentChange;
	}

	/**
	 * Set {@link #isSilentChange}
	 */
	private void setSilentChange(final boolean isSilentChange) {
		this.isSilentChange = isSilentChange;
	}

	/**
	 * Get {@link #isProcessing}
	 */
	private synchronized boolean isProcessing() {
		return isProcessing;
	}

	/**
	 * Set {@link #isProcessing}
	 */
	private synchronized void setProcessing(final boolean isProcessing) {
		this.isProcessing = isProcessing;
	}
}
