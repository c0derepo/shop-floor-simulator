/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package shop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.NumberBinding;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.util.Pair;
import main.Config;
import main.ThreadsSupplier;

/**
 * Constructs and manages {@link Checkout}s, a {@link CustomerQueue} for each checkout,
 * a {@link CustomerQueue} which serves as buffer queue and {@link QueuesManager}
 *
 * CheckoutsManager.java
 *
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 2 Nov 2018
 *
 */
public class CheckoutsManager {

	/**
	 *
	 */
	private final List<Checkout> checkouts;

	/**
	 *
	 */
	private final Set<CustomerQueue> queues;

	/**
	 * Buffer to hold customers that where not admitted to a checkout queue
	 */
	private final CustomerQueue bufferQueue;

	/**
	 *
	 */
	private final QueuesManager queueManager;

	/**
	 *
	 */
	private final Config config;

	/**
	 *
	 */
	private boolean isBalancingOn;

	/**
	 * Represents total customers in checkout queue
	 */
	private final SimpleIntegerProperty numOfQueuesCustomers;

	/**
	 * Represents total customers served by cashiers
	 */
	private final SimpleIntegerProperty numOfCashierCustomers;

	/**
	 * Represents total customers in queue and served by cashiers
	 */
	private final SimpleIntegerProperty numOfCheckoutCustomers;

	/**
	 *
	 */
	private Runnable checkoutsCapacityCheck;

	/**
	 * Time of last checkouts load-check
	 */
	private long lastCheckoutsCapacityCheckTime;

	/**
	 * Factor by which to shorten time checkouts load-check<br/>
	 * Valid values > 1
	 */
	private int shrortenByFactor = 1;

	/**
	 * Ignore capacity shortage / excess below this value. Value of
	 * {@value #MIN_CAPACITY_CAHNGE} represents {@value #MIN_CAPACITY_CAHNGE} checkout.
	 */
	public static final double MIN_CAPACITY_CAHNGE = 0.8;

	/**
	 *
	 */
	private boolean isTerminate;

	// ///////////////////////////////////////////////////////////
	// ////////////////// Constructor ////////////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 *
	 */
	public CheckoutsManager(final FloorCustomers customers, final Config config) {

		this.config = config;
		isTerminate = false;

		final int numberOfCheckOuts = config.getMaxNumberOfCheckouts();
		checkouts = new ArrayList<>(numberOfCheckOuts);

		queues = Collections.synchronizedSet(new HashSet<>());

		bufferQueue = new CustomerQueue(config.getSBufferQueueCapacity());
		bufferQueue.setBlocked(false);

		queueManager = new QueuesManager(checkouts, customers, bufferQueue, config);

		numOfQueuesCustomers = new SimpleIntegerProperty(0);
		numOfCashierCustomers = new SimpleIntegerProperty(0);

		//add binding of all queues customers
		NumberBinding totalQueus = Bindings.add(0,  new SimpleIntegerProperty(0)); //empty binding
		NumberBinding totalCheckouts =  Bindings.add(0,  new SimpleIntegerProperty(0)); //empty bindingnew When
		for(int cOut = 0; cOut < numberOfCheckOuts; cOut ++) {

			final Checkout checkout = new Checkout(buildQueue());
			//bind queue number of customers
			totalQueus = Bindings.add(totalQueus, checkout.getQueue().numOfQueCustomers());

			//bind checkout number of customers
			totalCheckouts = Bindings.add(totalCheckouts, Bindings.when(checkout.isBusyProperty()).then(1).otherwise(0) );

			checkout.isActive().addListener(checkOutListener());
			checkouts.add(checkout);
		}

		numOfQueuesCustomers.bind(totalQueus);
		numOfCashierCustomers.bind(totalCheckouts);

		//bind to add queues customers and checkout customers
		numOfCheckoutCustomers = new SimpleIntegerProperty(0);
		numOfCheckoutCustomers.bind(Bindings.add(numOfQueuesCustomers,numOfCashierCustomers));

		monitorBufferQueue();
	}

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 *
	 */
	private CustomerQueue buildQueue() {

		final CustomerQueue queue = new CustomerQueue(config.getQueueCapacity());
		queues.add(queue);

		return queue;
	}

	/**
	 * Activate min number of checkouts, as defined by {@link Config#getMinNumberOfCheckouts()}
	 */
	void start() {

		Collections.shuffle(checkouts);

		isBalancingOn = false;  //ignore checkout add, do not run balancing on start
		for(final Checkout checkout : checkouts) {

			if(activeCheckoutsCount() >= config.getMinNumberOfCheckouts()) {
				break;
			}
			checkout.start();
		}

		isBalancingOn = true;
		queueManager.start();

		periodCheckoutsCapacityCheck();
	}

	/**
	 *
	 */
	private void monitorBufferQueue() {

		new MonitorBufferQueue(bufferQueue.numOfQueCustomers(), config.autoPilotProperty());
	}

	/**
	 *
	 *
	 */
	boolean startCheckout() {

		Collections.shuffle(checkouts);
		for(final Checkout checkout : checkouts) {
			if( checkout.isOff()) {
				checkout.start();
				return true;
			}
		}
		return false;
	}

	/**
	 * Block queue from getting new entries and stop queue manager
	 */
	void stop() {

		isTerminate = true;
		for(final Checkout checkout : checkouts) {
			checkout.off();
		}

		queueManager.stop();
	}

	/**
	 * Listen to checkout becoming active or in active
	 */
	ChangeListener<Boolean> checkOutListener() {

		//respond to change in active checkouts
		return (obs, oldValue, newValue)->	checkQueuesLoads();
	}

	/**
	 * Check queue balance
	 */
	private void checkQueuesLoads() {

		if(queueManager != null && isBalancingOn  ) {
			new Thread(()->
			queueManager.queuesLoadsCheck() ).start();
		}
	}

	/**
	 *
	 */
	int activeCheckoutsCount() {

		return (int) activeCheckouts().count();
	}

	/**
	 *
	 */
	private Stream<Checkout> activeCheckouts() {

		return  checkouts.stream().filter(c -> c.isActive().get());
	}

	/**
	 *
	 */
	int nonActiveCheckoutsCount() {

		return (int) nonActiveCheckouts().count();
	}

	/**
	 *
	 */
	private Stream<Checkout> nonActiveCheckouts() {

		return  checkouts.stream().filter(c -> ! c.isActive().get());
	}

	/**
	 *
	 */
	Pair<Integer,Integer> minMaxLoadedQueues() {

		final Pair<CustomerQueue, CustomerQueue> minMax = queueManager.minMaxLoadedQueues();
		return new Pair<>(minMax.getKey().numOfQueCustomers().get(),
												minMax.getValue().numOfQueCustomers().get());
	}

	/**
	 * Periodic check and change number of active checkouts based on load
	 */
	private void periodCheckoutsCapacityCheck() {

		isTerminate = false;
		checkoutsCapacityCheck = ()-> {
			Thread.currentThread().setName("priodicCheckoutsCapacityCheck");
			// stop condition
			if( isTerminate ) 	return;

			if(config.autoPilotProperty().get() && isTimeForNextLoadCheck()) {
				checkoutsCapacityCheck();
			}

			//schedule next balance
			ThreadsSupplier.getExecutor().schedule(checkoutsCapacityCheck,
												intervalBetweenCheckoutsLoadChecks(),TimeUnit.MILLISECONDS);
		};

		//initial invoke with delay
		ThreadsSupplier.getExecutor().schedule(checkoutsCapacityCheck,
													intervalBetweenCheckoutsLoadChecks(), TimeUnit.MILLISECONDS);
	}

	/**
	 * Check and change number of active checkouts based on load
	 *
	 * Capacity shortage / excess below default value as defined by
	 * {@link #MIN_CAPACITY_CAHNGE} are ignored
	 */
	void checkoutsCapacityCheck() {

		 checkoutsCapacityCheck(MIN_CAPACITY_CAHNGE);
	}

	/**
	 * Check and change number of active checkouts based on load
	 * @param minCapacityChange
	 * Ignore capacity shortage / excess below this value. for example if value is 0.5
	 * capacity changes of plus or minus half checkout are ignored
	 */
	private void checkoutsCapacityCheck(final double minCapacityChange) {

		final Future<Void> e = ThreadsSupplier.getExecutor().submit(()-> {

			lastCheckoutsCapacityCheckTime  = System.currentTimeMillis();

			Thread.currentThread().setName("checkoutsCapacityCheck");
			final double neededCapacity = capacityNeeded();

			boolean capacityNeedStaisfied = true;
			if( activeCheckoutsCount() < config.getMinNumberOfCheckouts() || //if below minimum, add
					//needed capacity change is over threshold and more capacity is available
					neededCapacity > minCapacityChange && activeCheckoutsCount() < config.getMaxNumberOfCheckouts() ){

				capacityNeedStaisfied = addCapacity(neededCapacity);

			}else if (neededCapacity < - minCapacityChange && activeCheckoutsCount() > config.getMinNumberOfCheckouts())
														{//needed capacity change is over threshold and capacity is above min
				shutNotBusyCheckouts();
				capacityNeedStaisfied = reduceCapacity(neededCapacity);
			}else{
				shutNotBusyCheckouts();
			}

			//shorten time to next check by increasing factor from 1 > 2 > 3 > 4 > 6
			final int factor = capacityNeedStaisfied ? 1 : (int)Math.max(2, getShrortenByFactor() * 1.5) ;
			setShrortenByFactor (Math.min(10, factor) );//max factor 10
			return null;
		});

		try {
			e.get(); //wait for thread to finish before returning
		} catch (InterruptedException | ExecutionException ex) {ex.printStackTrace();}
	}

	/**
	 * @return true if needed capacity satisfied
	 */
	private boolean  addCapacity(double neededCapacity) {

		assert neededCapacity >= 0;

		final List<Checkout> nonActiveCheckouts = nonActiveCheckouts().collect(Collectors.toList());
		boolean opened = false;
		//try turning on stopping / stopped checkout
		for( final Checkout checkout : nonActiveCheckouts){

			if(checkout.stoppingProperty().get() || checkout.isStopped()){
				checkout.start();
				opened = true;
				//log to console turned from stop to on
				//System.out.println("\n"+".+.");
				break;
			}
		}

		if(! opened){

			//try turning on any checkout
			for( final Checkout checkout : nonActiveCheckouts){
					checkout.start();
					opened = true;
					//log to console turned from off to on
					//System.out.println("\n"+".++.");
					break;
			}
		}

		neededCapacity = opened ? neededCapacity -1 : neededCapacity ;
		return neededCapacity < MIN_CAPACITY_CAHNGE;
	}

	/**
	 *Shut down stopped not busy checkouts
	 */
	private void shutNotBusyCheckouts() {

		nonActiveCheckouts().filter(Checkout::isStopped).filter( c ->  ! c.isBusyProperty().get()).
		forEach(Checkout::off);
	}

	/**
	 *@return true if needed capacity satisfied
	 */
	private boolean reduceCapacity(double neededCapacity) {

		assert neededCapacity <= 0;

		boolean closed = false;
		//try turning off stopping / stopped checkout
		for( final Checkout checkout : nonActiveCheckouts().collect(Collectors.toList())){

			if(checkout.stoppingProperty().get() || checkout.isStopped()){
				checkout.off();
				closed = true;
				//log to console turned from stop to off
				//System.out.println("\n"+".-.");
				break;
			}
		}

		if(! closed){

			//try turning off any checkout
			for( final Checkout checkout : activeCheckouts().collect(Collectors.toList())){
					checkout.stop();
					closed = true;
					//log to console turned from on to off
					//System.out.println("\n"+".--.");
					break;
			}
		}

		neededCapacity = closed ? neededCapacity + 1 : neededCapacity ;
		return neededCapacity > - MIN_CAPACITY_CAHNGE;
	}

	/**
	 * How many more checkouts are needed to have queue in their optimal size, as defined by
	 * {@link Config#getOptimalQueueSize()}<br/>
	 * Negative number indicates excess capacity (number of checkouts to stop / shut down)
	 */
	private double capacityNeeded(){

		return
			(bufferQueue.numOfQueCustomers().doubleValue()+ numOfQueuesCustomers.doubleValue() )
											/ config.getOptimalQueueSize()	- activeCheckoutsCount();
	}

	/**
	 *
	 */
	private boolean isTimeForNextLoadCheck() {

		return lastCheckoutsCapacityCheckTime == 0 || System.currentTimeMillis()
								- lastCheckoutsCapacityCheckTime > intervalBetweenCheckoutsLoadChecks() ;
	}

	/**
	 *@return
	 * milliseconds
	 */
	private long intervalBetweenCheckoutsLoadChecks() {

		//1H = 60min x 60sec x 1000mills
		return (long) (3600000 / config.getCheckoutsLoadChecksPerHour()) /getShrortenByFactor();
	}

	// ///////////////////////////////////////////////////////////
	// ////////////////// Getters, Setters ///////////////////////
	// ///////////////////////////////////////////////////////////
	/**
	 * Get {@link #queues}
	 */
	Set<CustomerQueue> getQueues() {

		return queues;
	}

	/**
	 * Get {@link #checkouts}
	 */
	List<Checkout> getCheckouts() {

		return checkouts;
	}

	/**
	 * Get {@link #queueManager}
	 */
	QueuesManager getQueueManager() {

		return queueManager;
	}

	/**
	* Get {@link #bufferQueue}
	*/
	CustomerQueue getBufferQueue() {

		return bufferQueue;
	}

	/**
	 * Get {@link #numOfCheckoutCustomers}
	 */
	SimpleIntegerProperty numOfCheckoutCustomers() {
		return numOfCheckoutCustomers;
	}

	/**
	 * Number of customers in the buffer queue
	 */
	SimpleIntegerProperty numOfBufferCustomers() {

		return getBufferQueue().numOfQueCustomers();
	}

	/**
	* Get {@link #shrortenByFactor}
	*/
	int getShrortenByFactor() {
		return shrortenByFactor;
	}

	/**
	* Set {@link #shrortenByFactor}
	*/
	void setShrortenByFactor(final int shrortenByFactor) {
		this.shrortenByFactor = shrortenByFactor > 0 ? shrortenByFactor : 1;
	}

	/**
	* Get {@link #isTerminate}
	*/
	private boolean isTerminate() {
		return isTerminate;
	}

	/**
	* Set {@link #isTerminate}
	*/
	void setTerminate(final boolean isTerminate) {
		this.isTerminate = isTerminate;
	}

	// ///////////////////////////////////////////////////////////
	// /////////////// ControlCheckouts class ////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 *
	 */
	final class MonitorBufferQueue {

		private final SimpleIntegerProperty bufferCustomersProperty;
		private final SimpleBooleanProperty autoPilotProperty;
		private ChangeListener<Number> bufferCustomersListener;

		/**
		 *
		 */
		public MonitorBufferQueue(final SimpleIntegerProperty numOfFloorCustomersProperty,
													final SimpleBooleanProperty autoPilotProperty) {
			bufferCustomersProperty = numOfFloorCustomersProperty;
			this.autoPilotProperty = autoPilotProperty;
			listen();
		}

		private void listen() {
			bufferCustomersListener = (obs, oldValue, newValue) ->{

				if(isTerminate() ) return;

				// respond to buffer change from 0 to 1
				if(oldValue.intValue() <= 0 && newValue.intValue() >= 1 ){

					//if there is high capacity shortage or all checkouts are off
					if(activeCheckoutsCount() == 0 ||  capacityNeeded() > 3* MIN_CAPACITY_CAHNGE) {
						checkoutsCapacityCheck();
					}
				}
			};

			if(autoPilotProperty.get()) {
				bufferCustomersProperty.addListener(bufferCustomersListener);
			}

			autoPilotProperty.addListener(
					(obs,oldValue, newValue) ->{
						if(newValue) {
							if(bufferCustomersProperty.get() > 0) {
								checkoutsCapacityCheck();
							}
							bufferCustomersProperty.addListener(bufferCustomersListener);

						} else {
							bufferCustomersProperty.removeListener(bufferCustomersListener);
						}
					}
			);
		}
	}
}
