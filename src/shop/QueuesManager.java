package shop;

import java.util.List;
import java.util.OptionalInt;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javafx.util.Pair;
import main.Config;
import main.ThreadsSupplier;;

/**
 * Loads queues (move customer from {@link FloorCustomers} ) periodically<br/>
 * Manages all queues, including buffer queue
 *
 * QueueManger .java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 20 Oct 2018
 *
 */
public class QueuesManager {

	/**
	 *
	 */
	private final List<Checkout> checkouts;

	/**
	 *
	 */
	private final FloorCustomers customers;

	/**
	 *
	 */
	private final  Config config;

	/**
	 *
	 */
	private boolean isTerminate;

	/**
	 *
	 */
	private Runnable loadQueue;

	/**
	 *
	 */
	private Runnable balanceQueue;

	/**
	 * A single Single Thread Executor to run periodic load of queues
	 */
	private final ScheduledExecutorService preidicLoadQueuService;

	/**
	 * A single Single Thread Executor to guarantee queuing of tasks and executing
	 * one by one
	 */
	private final ScheduledExecutorService queuesLoadsCheckService;

	/**
	 *
	 */
	private boolean isBalancing;

	/**
	 *
	 */
	private boolean isLoadingQueues;

	/**
	 * Buffer to hold customers that where not admitted to a checkout queue
	 */
	private final CustomerQueue bufferQueue;

	/**
	 * Time of last queues load check
	 */
	private long lastQueueLoadCheckTime;

	// ///////////////////////////////////////////////////////////
	// ////////////////// Constructor ////////////////////////////
	// ///////////////////////////////////////////////////////////
	/**
	 *
	 */
	public QueuesManager(final List<Checkout> checkouts, final FloorCustomers customers,
									final CustomerQueue bufferQueue, final Config config) {
		this.checkouts = checkouts;
		this.customers= customers;
		this.config = config;
		isTerminate = false;  isBalancing = false; isLoadingQueues = false;
		this.bufferQueue = bufferQueue;
		queuesLoadsCheckService = ThreadsSupplier.getSingleThreadExecutor();
		preidicLoadQueuService = ThreadsSupplier.getSingleThreadExecutor();
	}

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////
	/**
	 *
	 */
	void start() {
		periodicLoadQueues();
		periodicQueuesLoadCheck();
	}

	/**
	 *Stop adding customers to shop<br/>
	 *Stop balancing after queues are empty
	 */
	void stop() {

		isTerminate = true;
	}

	/**
	 * <pre>
	 * Periodically loads customers to queues by :
	 * - empty buffer queue
	 * - getting random customer from floor and adding to queue. If queues are not available
	 *   an attempt is made to add if to buffer-queue. Customer removed from floor only if add
	 *   is successful
	 *  </pre>
	 */
	private void periodicLoadQueues() {

		isTerminate = false;
		loadQueue = ()-> {

			Thread.currentThread().setName("periodicLoadQueues");
			if(! isLoadingQueues() ) {//skip if previous process did not end

				setLoadingQueues(true);

				try {
					//handle non empty blocked queue first
					emptyLockedNonEmptyQueues();
					emptyBufferQueue();

					if(! config.isJustOpened() || customers.numOfFloorCustomers().get() >
															config.getMinFloorCustomers()) {
						moveCustomersFromFloorToQueues();
					}
				} catch (final Exception ex) { ex.printStackTrace();
				}finally 	{ setLoadingQueues(false); }
			}

			//schedule next load
			if( ! isTerminate) {
				final long wait = CustomersManager.getRandomWaitBetweenCustomers
																(config.getLoadQueuesAccelerationFactor());
				preidicLoadQueuService.schedule(loadQueue, wait, TimeUnit.MILLISECONDS);
			}
		};

		preidicLoadQueuService.execute(loadQueue);
	}

	/**
	 *
	 */
	private void moveCustomersFromFloorToQueues() {

		boolean needToRemoveFromCustomers = false;
		final Customer customer = customers.getRandomCustomer();

		if( customer == null) return;
		assert customer.getBasket() != null : " customer basket should not be null";

		if( customer.getBasket().size() > 0) {//do only if customers has products

			//add to queue is customer has products
			boolean added = addToQueue(customer);
			if(! added) {
				added = addToBufferQueue(customer);
				//log to console move from floor to buffer queue succeeded / blocked
				//System.out.print(added ? ".<." : ".|<.");
			}else {
				//log to console move from floor to queue
				//System.out.print(".V.");
			}
			needToRemoveFromCustomers = added;

		}else {
			needToRemoveFromCustomers = true; //remove customers that has no products
		}

		if(needToRemoveFromCustomers) {
			customers.removeCustomer(customer);
		}
	}

	/**
	 *
	 */
	private void periodicQueuesLoadCheck() {

		isTerminate = false;
		balanceQueue = ()-> {
			Thread.currentThread().setName("periodicQueuesLoadCheck");
			// stop condition
			if( isTerminate ) 	return;

			if(isTimeForNextLoadCheck()) {
				queuesLoadsCheck();
			}

			//schedule next balance
			ThreadsSupplier.getExecutor().schedule(balanceQueue, intervalBetweenQueuesLoadChecks(),
					TimeUnit.MILLISECONDS);
		};

		//initial invoke with delay
		ThreadsSupplier.getExecutor().schedule(balanceQueue,
				intervalBetweenQueuesLoadChecks() * 5, TimeUnit.MILLISECONDS); //5 times the normal interval
	}

	/**
	 * Balance queues, handle locked customers
	 */
	void queuesLoadsCheck() {

		queuesLoadsCheckService.execute(

			()-> {
				Thread.currentThread().setName("queuesLoadsCheck");
				lastQueueLoadCheckTime  = System.currentTimeMillis();

				//handle non empty blocked queue first
				emptyLockedNonEmptyQueues();

				//move customers to less loaded queues before empty buffer queue
				balanceQueues();
				emptyBufferQueue();
			}
		);
	}

	/**
	 *
	 */
	private synchronized void emptyLockedNonEmptyQueues() {

		//try adding to other free queues
		boolean blockedQueuesEmptied = addToQueue(getLockedNonEmptyQueues());
		//if need try adding to buffer
		//todo not so good: locked customers move to the end of the buffer queue
		if(! blockedQueuesEmptied) {
			blockedQueuesEmptied = addToBufferQueue(getLockedNonEmptyQueues());
		}
	}

	/**
	 * Iterate over queues to equalize thier size
	 */
	private void balanceQueues() {

		if(isBalancing()) return;

		setBalancing(true);
		int counter = 0;

		try {
			while(true) {

				final Pair<CustomerQueue, CustomerQueue> minMaxQueues = minMaxLoadedQueues();

				if(minMaxQueues.getValue() == null || minMaxQueues.getKey() == null ) { break ;}

				final double delta = minMaxQueues.getValue().numOfQueCustomers().get()
													- minMaxQueues.getKey().numOfQueCustomers().get() ;
				if(delta < 2 ) {

					setBalancing(false);
					//log to console number of customers moved between queues
					//System.out.println(".<"+ counter + ">.");
					return;
				}

				//get customers from max loaded queue and move it to min loaded queue
				for ( int i = 0; i < Math.ceil(delta / 2) ; i++) {

					//get last customer from highest loaded
					final Customer lastCustomer = minMaxQueues.getValue().getLast();

					if(lastCustomer != null) {
						//if fail to add to min loaded queue
						if( ! minMaxQueues.getKey().addCustomer(lastCustomer) ) {

							//if fail try put it in any queue, including buffer
							if(! addToQueue(lastCustomer) && ! addToBufferQueue(lastCustomer) ) {

								setBalancing(false);
								//both fail
								throw new IllegalStateException(lastCustomer + " could not be added to queue");
							}
						}else {
							counter++;
						}

					}else { //removal from max loaded queue failed
						setBalancing(false);
						return;
					}
				}
			}

		} catch (final Exception ex) { ex.printStackTrace();
		} finally  {setBalancing(false);}
	}

	/**
	 *
	 */
	private synchronized void emptyBufferQueue() {

		if(bufferQueue.numOfQueCustomers().get() < 1  ) return;
		int counter = 0;

		while(true) {
			final boolean transfered = CustomerQueue.transferBetweenQueues(bufferQueue, getFreeQueue());
			if (! transfered) {
				break;
			}
			counter++;
		}

		if(counter >0 ) {
			//log to console number of customers moved from buffer to queues
			//System.out.println(".V"+ counter + "V.");
		}
	}

	/**
	 * Remove {@link Customer} from {@link CustomerQueue} and add to free queue, if possible<br/>
	 * If add to queue fails, customers will not be removed from their origin queue.
	 */
	private boolean addToQueue(final List<CustomerQueue> lockedQueues) {

		boolean success = true;

		for(final CustomerQueue queue : lockedQueues) {

			while (true) {
				final Customer customer = queue.peek();
				if (customer == null) {	break;}

				if( addToQueue(customer)) {
					queue.getNext(); //remove from queue
				}else {
					break;
				}
			}
			if(! queue.isEmpty() ) {
				success = false;
			}
		}

		return success;
	}

	/**
	 * Adds {@link Customer} to the queue with the highest free capacity <br/>
	 * @param customer
	 * It is caller responsibility to check that queue does not
	 * @return
	 * If add fails: returns false
	 */
	synchronized boolean addToQueue(final Customer customer) {

		final CustomerQueue queue = getFreeQueue();
		if(queue == null)	return false;

		return  queue.addCustomer(customer);
	}

	/**
	 * Remove {@link Customer} from {@link CustomerQueue} and add buffer queue, if possible<br/>
	 * If buffer is full, customers will not be removed/
	 */
	private boolean addToBufferQueue(final List<CustomerQueue> lockedQueues) {

		boolean success = true;

		for(final CustomerQueue queue : lockedQueues) {

			while (true) {
				final Customer customer = queue.peek();
				if (customer == null) {	break;}

				if( addToBufferQueue(customer)) {
					queue.getNext(); //remove from queue
				}else {
					break;
				}
			}
			if(! queue.isEmpty() ) {
				success = false;
			}
		}

		return success;
	}

	/**
	 *
	 */
	private boolean addToBufferQueue(final Customer customer) {

		return bufferQueue.addCustomer(customer);
	}

	/**
	 *Returns the queue with the highest free capacity <br/>
	 *Returns null if all full, unavailable or blocked
	 */
	CustomerQueue getFreeQueue() {

		final CustomerQueue queue = minMaxLoadedQueues().getKey(); // queue with min load
		return queue == null || queue.remainingCapacity() <= 0 ?  null : queue;
	}

	/**
	 *@return
	 * Blocked, shut down queues, which are not empty
	 */
	private List<CustomerQueue> getLockedNonEmptyQueues() {

		return checkouts.stream().
				filter(c -> ! c.stoppingProperty().get()).   //filter out checkouts in stopping process
				map(Checkout::getQueue).         //get queues
				filter(q ->  q.isBlocked()).     //get blocked queues
				filter(q -> ! q.isEmpty()).      //get non empty
				collect(Collectors.toList());
	}

	/**
	 * Get number of customers of highest loaded queue
	 * @param ignoreBlocked
	 * Set to true to ignore blocked queue
	 */
	OptionalInt maxQueueLoad(final boolean ignoreBlocked) {
		return queueLoads(ignoreBlocked).max();
	}

	/**
	 * Get number of customers of lowest loaded queue
	 * @param ignoreBlocked
	 * Set to true to ignore blocked queue
	 */
	OptionalInt minQueueLoad(final boolean ignoreBlocked) {
		return queueLoads(ignoreBlocked).max();
	}

	/**
	 * Stream of queue loads (number of customers)
	 * @param ignoreBlocked
	 * Set to true to ignore blocked queue
	 */
	private IntStream queueLoads(final boolean ignoreBlocked) {

		return checkouts.stream().
				map(Checkout::getQueue).
				filter( c ->  (  ignoreBlocked ?  true :  !c.isBlocked() ) ).
				mapToInt( q -> q.numOfQueCustomers().get());
	}

	/**
	 *<pre>
	 *@return {@link Pair#getKey()} and {@link Pair#getValue()} hold the lowest load, and highest
	 * loaded {@link CustomerQueue} respectively
	 * Blocked queues (( {@link CustomerQueue#isBlocked()} ) are ignored when searching for lowest
	 * loaded queue. Blocked queues are <b>not ignored</b> when searching for highest load queue.
	 * If the only available queue is blocked key (min) returns null and  value returns
	 * If queues are not available or all blocked both key and and value return null a {@link CustomerQueue}.
	 * When all queues are equally loaded, key and and value return the same {@link CustomerQueue}.
	 * </pre>
	 * Used by loadQueue thread as well as balanceQueues
	 */
	synchronized Pair<CustomerQueue, CustomerQueue> minMaxLoadedQueues() {

		CustomerQueue maxLoadedQueue = null, minLoadeQueue = null;
		int maxCustomers = 0, minCustomers = Integer.MAX_VALUE;

		for(final Checkout checkOut : checkouts) {

			final CustomerQueue queue = checkOut.getQueue();

			final int numberOfCustomers = queue.numOfQueCustomers().get();
			//keep if max
			if(numberOfCustomers >= maxCustomers) {
				maxCustomers = numberOfCustomers;
				maxLoadedQueue = queue;
			}
			//keep if min
			if(! queue.isBlocked() && numberOfCustomers <= minCustomers) {
				minCustomers = numberOfCustomers;
				minLoadeQueue = queue;
			}
		}

		return new Pair<>(minLoadeQueue , maxLoadedQueue);
	}

	/**
	 *@return
	 */
	private boolean isTimeForNextLoadCheck() {

		return lastQueueLoadCheckTime == 0 ||
				System.currentTimeMillis() - lastQueueLoadCheckTime > intervalBetweenQueuesLoadChecks() ;
	}

	/**
	 *
	 *@return
	 * milliseconds
	 */
	private long intervalBetweenQueuesLoadChecks() {

		return 3600000 / config.getQueuesLoadChecksPerHour(); //1H = 60min x 60sec x 1000mills
	}

	// ///////////////////////////////////////////////////////////
	// ////////////////// Getters, Setters ///////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 * Get {@link #isBalancing}
	 */
	private synchronized boolean isBalancing() {

		return isBalancing;
	}

	/**
	 * Set {@link #isBalancing}
	 */
	private synchronized void setBalancing(final boolean isBalancing) {

		this.isBalancing = isBalancing;
	}

	/**
	 * Get {@link #isLoadingQueues}
	 */
	private synchronized boolean isLoadingQueues() {

		return isLoadingQueues;
	}

	/**
	 * Set {@link #isLoadingQueues}
	 */
	private synchronized void setLoadingQueues(final boolean isLoadingQueues) {

		this.isLoadingQueues = isLoadingQueues;
	}
}
