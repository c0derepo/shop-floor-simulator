package shop;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import main.Config;
import wait.Wait;

public class Customer implements Runnable {

	/**
	 * Collection of products
	 */
	private final List<Product> basket;

	/**
	 * Unique id
	 */
	private final int customerId;

	/**
	 *
	 */
	private static int uid = 0;

	/**
	 * stop processing customer flag
	 */
	private final boolean isTerminate;

	/**
	 *
	 */
	private final int minProducts = 1, maxProducts = 20;

	/**
	 *
	 */
	private final Config config;

	/**
	 *
	 */
	public Customer(final Config config)  {
		this.config = config;
		customerId = uid++;
		isTerminate = false;
		basket =  getProducts(Random.inRange(minProducts, maxProducts));
	}

	/**
	 *
	 */
	private List<Product> getProducts(final int numberOfProducts) {

		final List<Product> basket = new ArrayList<>();
		for(int counter = 0; counter < numberOfProducts; counter ++ ) {

			basket.add(new Product());
		}

		return basket;
	}

	/**
	 *
	 */
	@Override
	public void run()   {
		process();
	}

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 *
	 */
	private void process() {

		Wait.millis( (long) (getProcessTime() / config.getCheckoutSpeedFactor())  );
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		return "Customer "+ getCustomerId();
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return 31 +17*customerId;
	}

	// ///////////////////////////////////////////////////////////
	// ////////////////// Getters, Setters ///////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 * Get total basket process time, milliseconds
	 */
	int getProcessTime() {

		return 	basket.stream().collect(Collectors.
				summingInt(p -> p.getTotalProcessTime()));
	}

	/**
	 * Get {@link #basket}
	 */
	List<Product> getBasket() {
		return basket;
	}

	/**
	 * Get {@link #customerId}
	 */
	int getCustomerId() {
		return customerId;
	}
}