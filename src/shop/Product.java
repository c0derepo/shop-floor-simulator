/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package shop;

/**
 * Product.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 19 Oct 2018
 *
 */
public class Product {

	/**
	 * Processing time per single product in milliseconds<br/>
	 */
	private int processTime;

	/**
	 * Max processing time per single product in milliseconds<br/>
	 * max should be > min > 0
	 */
	private int minProcessTime = 1000, maxProcessTime = 5000;

	/**
	 * Max processing time per any {@link #qty} of this product in milliseconds<br/>
	 */
	private final int processTimeCieling = 10000;

	/**
	 *
	 */
	private final int minItemsPerProduct = 1, maxItemsPerProduct = 6;

	/**
	 * Number of items from this product
	 */
	private final int qty;

	// ///////////////////////////////////////////////////////////
	// ////////////////// Constructor ////////////////////////////
	// ///////////////////////////////////////////////////////////
	Product() {

		processTime = Random.inRange(minProcessTime, maxProcessTime);
		qty = Random.inRange(minItemsPerProduct, maxItemsPerProduct);
	}

	// ///////////////////////////////////////////////////////////
	// ////////////////// Getters, Setters ///////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	* Get sum of {@link #processTime} for the {@link #qty} of products
	*/
	int getTotalProcessTime() {
		return Math.min(processTime * qty , processTimeCieling );
	}

	/**
	* Get {@link #processTime}
	*/
	int getProcessTime() {
		return processTime;
	}

	/**
	* Set {@link #processTime}
	*/
	void setProcessTime(final int processTime) {
		this.processTime = processTime;
	}

	/**
	* Get {@link #minProcessTime}
	*/
	int getMinProcessTime() {
		return minProcessTime;
	}

	/**
	* Get {@link #maxProcessTime}
	*/
	int getMaxProcessTime() {
		return maxProcessTime;
	}

	/**
	* Set {@link #minProcessTime}
	*/
	void setMinProcessTime(final int minProcessTime) {
		this.minProcessTime = minProcessTime;
	}

	/**
	* Set {@link #maxProcessTime}
	*/
	void setMaxProcessTime(final int maxProcessTime) {
		this.maxProcessTime = maxProcessTime;
	}
}
