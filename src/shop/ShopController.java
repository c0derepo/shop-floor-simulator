package shop;


import java.util.List;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.NumberBinding;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import main.Config;

/**
 * ShopController.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 19 Oct 2018
 *
 */
public class ShopController {

	/**
	 * Checkout states
	 */
	public enum ShopState{
		/**
		 * Shop is open
		 */
		OPEN,
		/**
		 * Shop is open, doors closed
		 */
		DOORS_CLOSED,
		/**
		 * Shop is closed
		 */
		CLOSED;

		@Override
		public String toString(){

			return super.toString().replaceAll("_", " ");
		}
	}

	/**
	 * High threshold of 0.9 means respond when capacity increases over 90%
	 */
	private static final double FULL_HIGH_TH = 0.9;

	/**
	 * Low threshold of 0.8 means respond when capacity decreases below 80%
	 */
	private static final double FULL_LOW_TH = 0.8;

	/**
	 *
	 */
	private CheckoutsManager checkoutsManager;

	/**
	 *
	 */
	private CustomersManager customerManager;

	/**
	 *
	 */
	private FloorCustomers customers;

	/**
	 *
	 */
	private final Config config;

	/**
	 *
	 */
	private SimpleObjectProperty<ShopState> state;

	/**
	 * Represents total shop customers: floor + queues + buffer queue
	 */
	private SimpleIntegerProperty numOfShopCustomers;

	/**
	 *
	 */
	private CustomerQueue bufferQueue;

	/**
	 * Stop automatic door control
	 */
	private boolean isStopAutoDoors;

	/**
	 * Temp listener to {@link #numOfShopCustomers()}
	 */
	private ChangeListener<Number> listener;

	// ///////////////////////////////////////////////////////////
	// ////////////////// Constructor ////////////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 *
	 */
	public ShopController(final Config config){
		this.config = config;
		init();
		controlDoor();
	}

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 *
	 */
	private void init() {

		customers = new FloorCustomers(config);

		checkoutsManager = new CheckoutsManager(customers, config);

		customerManager = new CustomersManager(customers, config);
		bufferQueue = checkoutsManager.getBufferQueue();

		state = new SimpleObjectProperty<>(ShopState.CLOSED);

		numOfShopCustomers = new SimpleIntegerProperty(0);

		//add binding of all queues customers
		NumberBinding total = Bindings.add(customers.numOfFloorCustomers(), checkoutsManager.numOfCheckoutCustomers());

		//add binding of buffer queue customers
		total = Bindings.add(total, checkoutsManager.numOfBufferCustomers());

		numOfShopCustomers.bind(total);
	}

	/**
	 *
	 */
	public void toggleShopState() {

		switch(state.get()) {

			case OPEN:
				closeDoors();
				return ;

			case DOORS_CLOSED:
				openDoors();
				return ;

			case CLOSED: default:
				openShop();
				return ;
		}
	}

	/**
	 *
	 */
	void openShop() {

		state.set(ShopState.OPEN);
		config.setJustOpened(true);
		checkoutsManager.start();
		isStopAutoDoors = false;
		openDoors();
	}

	/**
	 * Start adding {@link Customer} to shop </br>
	 * Opening doors is valid only when state is {@link ShopState#OPEN} or {{@link ShopState#DOORS_CLOSED}
	 */
	void openDoors() {

		if(state.get() != ShopState.OPEN && state.get() != ShopState.DOORS_CLOSED) return;
		if(state.get() == ShopState.DOORS_CLOSED) {
			state.set(ShopState.OPEN);
		}

		customerManager.start();
		//log to console open doors
		//System.out.println("\n"+".|O|.");
	}

	/**
	 *
	 */
	public void closeShop() {

		config.setJustOpened(false); //in case opening was very short and flag was not turned off

		//disable auto pilot door control
		isStopAutoDoors = true;

		closeDoors(); //in case doors are open

		if(isShopEmpty()){
			shutDown();
		}else{

			listener = (obs, oldValue, newValue)->{
				if(newValue.intValue() == 0) {
					numOfShopCustomers.removeListener(listener);
					shutDown();
				}
			};
			numOfShopCustomers.addListener(listener );
		}
	}

	/**
	 * Finish shop-close when shop is empty
	 */
	private void shutDown(){

		//stop checkouts
		checkoutsManager.stop();

		Platform.runLater(()->	{
			state.set(ShopState.CLOSED);
		});
	}

	/**
	 * Stop adding {@link Customer} to shop </br>
	 * Closing doors is valid only when state is {@link ShopState#OPEN}
	 */
	void closeDoors() {

		if(state.get() == ShopState.DOORS_CLOSED) return;
		state.set(ShopState.DOORS_CLOSED);
		customerManager.stop();
		//log to console close doors
		//System.out.println("\n"+".|C|.");
	}

	/**
	 * Controls door open - close
	 */
	private void controlDoor() {
		//control door / floor customers
		new ControlDoor(customers.numOfFloorCustomers(), config.autoPilotProperty());
	}

	/**
	 * Respond to shop capacity changes by opening / closing doors
	 * @param loadFactor
	 * range of 0-1 (0-100%)
	 */
	private void controlDoor(final double loadFactor) {

		if( isStopAutoDoors() )  return;

		if(loadFactor <= FULL_LOW_TH  && state.get() == ShopState.DOORS_CLOSED) {
			Platform.runLater(()-> openDoors());
		}else if (loadFactor >= FULL_HIGH_TH  && state.get() == ShopState.OPEN) {
			Platform.runLater(()-> closeDoors());
			//check capacity of checkouts
			if(config.autoPilotProperty().get()) {
				checkoutsManager.checkoutsCapacityCheck();
			}
		}
	}

	/**
	 *
	 */
	private boolean isShopEmpty() {

		return numOfShopCustomers.get() == 0;
	}

	// ///////////////////////////////////////////////////////////
	// ////////////////// Getters, Setters ///////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 * Get {@link #customers}
	 */
	public FloorCustomers getCustomers() {

		return customers;
	}

	/**
	 * Delegates to {@link FloorCustomers#accumulatedNumOfCustomers()}
	 */
	public SimpleIntegerProperty accumulatedNumOfCustomers() {

		return customers.accumulatedNumOfCustomers();
	}

	/**
	 *
	 *Get {@link #numOfShopCustomers}
	 */
	public SimpleIntegerProperty numOfShopCustomers() {

		return numOfShopCustomers;
	}

	/**
	 * Get {@link #checkouts}
	 */
	public List<Checkout> getCheckouts() {

		return checkoutsManager.getCheckouts();
	}

	/**
	 * Delegates {@link CheckoutsManager#getBufferQueue()}
	 */
	public CustomerQueue getBufferQueue() {

		return bufferQueue;
	}

	/**
	* Get {@link #state}
	*/
	public SimpleObjectProperty<ShopState> shopStateProperty() {
		return state;
	}

	/**
	* Get {@link #isStopAutoDoors}
	*/
	private boolean isStopAutoDoors() {
		return isStopAutoDoors;
	}

	// ///////////////////////////////////////////////////////////
	// ////////////////// ControlDoor class //////////////////////
	// ///////////////////////////////////////////////////////////

	final class ControlDoor {

		private final SimpleIntegerProperty floorCustomersProperty;
		private final SimpleBooleanProperty autoPilotProperty;
		private ChangeListener<Number> floorCustomersListener;

		/**
		 *
		 */
		public ControlDoor(final SimpleIntegerProperty floorCustomersProperty,
												final SimpleBooleanProperty autoPilotProperty) {

			this.floorCustomersProperty = floorCustomersProperty;
			this.autoPilotProperty = autoPilotProperty;

			listen();
		}

		private void listen() {

			floorCustomersListener = (obs, oldValue, newValue) ->{
				controlDoor(newValue.doubleValue() / config.getShopCapacity() );
			};

			if(autoPilotProperty.get()) {
				floorCustomersProperty.addListener(floorCustomersListener);
			}
			autoPilotProperty.addListener(	(obs,oldValue, newValue) ->{
					//if auto pilot triggered when floor is full, and checkout not active or when floor
					//empty and doors closed, floorCustomersProperty may never change. invoke control
					controlDoor(floorCustomersProperty.get() / config.getShopCapacity() );

					if(newValue) {
						floorCustomersProperty.addListener(floorCustomersListener);
					} else {
						floorCustomersProperty.removeListener(floorCustomersListener);
					}
				}
			);
		}
	}
}