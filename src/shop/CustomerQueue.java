package shop;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * CustomerQueue.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 28 Oct 2018
 *
 */
public class CustomerQueue {

	/**
	 *
	 */
	private final int queueCapcity;

	/**
	 *
	 */
	private final BlockingQueue<Customer> queue;

	/**
	 *
	 */
	private final SimpleIntegerProperty numOfQueCustomers;

	/**
	 * Unique id
	 */
	private final int queueId;

	/**
	 *
	 */
	private static int uid = 0;

	/**
	 * Flag indicates if queue is blocked for new entries;
	 */
	private boolean isBlocked ;

	private CountDownLatch takeLock;
	private CountDownLatch putLock;
	private static final TimeUnit UNIT = TimeUnit.MICROSECONDS;

	private static final long DEFAULT_WAIT = 500;

	/**
	 * Time to wait for lock to end<br/>
	 * Initialized to {@value #DEFAULT_WAIT}
	 */
	private long waitForLockTimeOut;

	// ///////////////////////////////////////////////////////////
	// ////////////////// Constructor ////////////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 *
	 */
	CustomerQueue(final int queueCapcity){
		this.queueCapcity = queueCapcity;
		queue = new ArrayBlockingQueue<>(queueCapcity);
		numOfQueCustomers = new SimpleIntegerProperty(0);
		queueId = uid++;
		setBlocked(true);
		waitForLockTimeOut = DEFAULT_WAIT;
	}

	// ///////////////////////////////////////////////////////////
	// ///////////////////// Methods /////////////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 * Inserts customer if it is possible to do so immediately without violating capacity
	 * restrictions
	 * @param customer
	 * It is caller responsibility to check that queue does not
	 * contain customer
	 * @return
	 * true upon success and false if no space is currently available
	 */
	boolean addCustomer(final Customer customer) {

		if( ! waitForPutLock() ) return false;
		assert ! queue.contains(customer) : queue + " already contains "+ customer;
		if(isBlocked()) return false;

		final boolean added = queue.offer(customer);
		if(added) { updateNumberOfCustomers();}

		return added;
	}

	/**
	 * Inserts customer if it is possible to do so within timeOut (milliseconds) without violating
	 * capacity restrictions
	 * @param customer
	 * It is caller responsibility to check that queue does not
	 * contain customer
	 * @return
	 * true upon success and false if no space is currently available
	 */
	boolean addCustomer(final Customer customer, final long timeOut) {

		if( ! waitForPutLock(timeOut) ) return false;
		assert ! queue.contains(customer) : queue + " already contains "+ customer;
		if(isBlocked()) return false;

		final boolean added = queue.offer(customer);
		if(added) { updateNumberOfCustomers();}

		return added;
	}

	/**
	 *
	 */
	boolean removeCustomer(final Customer customer) {

		if( ! waitForTakeLock() ) return false;
		final boolean removed = queue.remove(customer);
		if(removed) { updateNumberOfCustomers();}

		return removed;
	}

	/**
	 * Delegates to {@link Queue#poll()}<br/>
	 * May return null if take-lock is applied
	 */
	Customer getNext() {

		if( ! waitForTakeLock() ) return null;
		final Customer customer = queue.poll();
		updateNumberOfCustomers();
		return customer;
	}

	/**
	 * Delegates to {@link Queue#peek()}
	 */
	Customer peek() {

		return queue.peek();
	}

	/**
	 * Delegates to {@link Queue#contains(Object)}
	 */
	boolean contains(final Customer customer) {

		return queue.contains(customer);
	}

	/**
	 *Delegates to {@link BlockingQueue#take()}<br/>
	 *Note if is blocked and empty this method will never return<br/>
	 *	 * May return null if take-lock is applied
	 *
	 */
	Customer waitForNext() {

		if( ! waitForTakeLock(Long.MAX_VALUE) ) return null;
		Customer customer = null;

		try {
			customer = queue.take();
		} catch (final InterruptedException ex) { ex.printStackTrace();}

		updateNumberOfCustomers();
		return customer;
	}

	/**
	 * Get tail of queue<br/>
	 * May return null if removal of tail fails
	 */
	Customer getLast() {

		if( ! waitForTakeLock() ) return null;
		final Customer[] array = copyToArray();
		final Customer customer = array[array.length - 1];

		if( ! removeCustomer(customer) )
			return null;
		return customer;
	}

	/**
	 *
	 */
	int remainingCapacity() {

		return getQueueCapcity() - numOfQueCustomers().get();
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		return "Queue "+ getQueueId();
	}

	/**
	 *
	 */
	private void updateNumberOfCustomers() {

		numOfQueCustomers().set(queue.size());
	}

	/**
	 * Enable adding customers to queue
	 */
	void start() {
		setBlocked(false);
	}

	/**
	 * Disable adding customers to queue
	 */
	void stop() {
		setBlocked(true);
	}

	/**
	 * Empties queue into a list
	 * Delegates to {@link ArrayBlockingQueue#drainTo(java.util.Collection)}<br/>
	 * May return null if take-lock is applied
	 */
	List<Customer> drainToList() {
		if( ! waitForTakeLock() ) return null;
		final List<Customer> list = new ArrayList<>();
		queue.drainTo(list);
		updateNumberOfCustomers();
		return list;
	}

	/**
	 *
	 */
	Customer[] copyToArray() {

		final Customer[] array = new Customer[numOfQueCustomers().get()] ;
		queue.toArray(array);
		return array;
	}

	/**
	 * <pre>
	 * Take head from source queue and add to it target queue
	 * Target queue must have free capacity and unblocked
	 * Source queue must have a no null head
	 * If any of the queue is locked, timeout (defined by {@link #getWaitForLockTimeOut()} applied
	 * Null safe
	 * <pre/>
	 */
	public static boolean transferBetweenQueues(final CustomerQueue source, final CustomerQueue target){
		try {

			if( target == null ||  source == null) return false;
			//obtain get and put locks
			if( target.isBlocked() ||  ! source.obtainTakeLock() || ! target.obtainPutLock() ) return false;

			final Customer customer = source.peek();
			if(customer == null || target.remainingCapacity() < 1) return false;

			target.getQueue().add(customer);   source.getQueue().remove(customer);
			target.updateNumberOfCustomers();  source.updateNumberOfCustomers();

			return true;
		} catch (final Exception ex) { ex.printStackTrace();
		}finally {
			if(source != null) {
				source.releaseTakeLock();
			}
			if( target != null ) {
				target.releasePutLock();
			}
		}
		return false;
	}

	/**
	 *
	 */
	boolean obtainTakeLock() {

		if( ! waitForTakeLock(getWaitForLockTimeOut()) ) return false;
		takeLock = new CountDownLatch(1);
		return true;
	}

	/**
	 *
	 */
	boolean releaseTakeLock() {

		try {
			if (takeLock != null){
				takeLock.countDown();
			}
			return true;
		} catch (final Exception ex) {	ex.printStackTrace();	}

		return false;
	}

	/**
	 *
	 */
	boolean obtainPutLock() {

		if( ! waitForPutLock(getWaitForLockTimeOut()) ) return false;
		putLock = new CountDownLatch(1);
		return true;
	}

	/**
	 *
	 */
	boolean releasePutLock() {

		try {
			if (putLock != null){
				putLock.countDown();
			}
			return true;
		} catch (final Exception ex) {	ex.printStackTrace();	}

		return false;
	}

	/**
	 *
	 */
	private boolean waitForTakeLock() {
		return waitForTakeLock(0);
	}

	/**
	 *
	 */
	private boolean waitForTakeLock(final long timeOut) {

		try {
			if (takeLock != null){
				takeLock.await(timeOut, UNIT);
				if(takeLock.getCount() > 0 ) return false; //still locked after wait
			}
			return true;
		} catch (final InterruptedException ex) {	ex.printStackTrace();}

		return false;
	}

	/**
	 *
	 */
	private boolean waitForPutLock() {
		return waitForPutLock(0);
	}

	/**
	 *
	 */
	private boolean waitForPutLock(final long timeOut) {

		try {
			if (putLock != null){
				putLock.await(timeOut, UNIT);
				if(putLock.getCount() > 0 ) return false; //still locked after wait
			}
			return true;
		} catch (final InterruptedException ex) {	ex.printStackTrace();}

		return false;
	}

	// ///////////////////////////////////////////////////////////
	// ////////////////// Getters, Setters ///////////////////////
	// ///////////////////////////////////////////////////////////

	/**
	 *
	 */
	public boolean isEmpty() {

		return queue.isEmpty();
	}

	/**
	 *
	 */
	public boolean isFull() {

		return numOfQueCustomers().get() == getQueueCapcity();
	}

	/**
	 * Get {@link #isBlocked}
	 */
	synchronized boolean isBlocked() {

		return isBlocked;
	}

	/**
	 * Set {@link #isBlocked}
	 */
	synchronized void setBlocked(final boolean isBlocked) {

		this.isBlocked = isBlocked;
	}

	/**
	 * Get {@link #queueCapcity}
	 */
	public int getQueueCapcity() {

		return queueCapcity;
	}

	/**
	* Get {@link #waitForLockTimeOut}
	*/
	protected long getWaitForLockTimeOut() {
		return waitForLockTimeOut;
	}

	/**
	* Set {@link #waitForLockTimeOut}
	*/
	protected void setWaitForLockTimeOut(final long waitForLockTimeOut) {
		this.waitForLockTimeOut = waitForLockTimeOut;
	}

	/**
	 * Get {@link #numOfQueCustomers}
	 */
	public SimpleIntegerProperty numOfQueCustomers() {

		return numOfQueCustomers;
	}

	/**
	* Get {@link #queue}
	*/
	private BlockingQueue<Customer> getQueue() {
		return queue;
	}

	/**
	 * Get {@link #queueId}
	 */
	public int getQueueId() {

		return queueId;
	}
}
