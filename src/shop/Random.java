/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package shop;

/**
 * Random.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 20 Oct 2018
 *
 */
public final class Random {

	private static java.util.Random rnd  = new java.util.Random();

	/**
	 * Returns a random number in 0-max range, both inclusive<br/>
	 * valid value: max >= 0;
	 */
	public static int upTo(int max) {

		return inRange(0, max);
	}

	/**
	 * Returns a random number in min-max range, both inclusive
	 * valid values: max >= min >= 0;
	 */
	public static int inRange(int min, int max) {

		return min + rnd.nextInt((max - min) + 1);
	}
}
