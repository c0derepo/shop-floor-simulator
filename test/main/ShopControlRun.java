/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package main;

import shop.ShopController;

/**
 * ShopControlRun.java
 *
 * @Author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 20 Oct 2018
 *
 */
public class ShopControlRun{

	public static void main(final String[] args) {

		final ShopController shop = new ShopController(new Config());

		shop.getCustomers().numOfFloorCustomers().addListener(
				(observable,oldValue, newValue) ->{
					System.out.println("getNumOfFloorCustomers() " + newValue);
				});

		new Thread(() -> {
			shop.toggleShopState();
		}).start();
	}
}
