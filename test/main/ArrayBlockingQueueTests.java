/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

/**
 *
 * ArrayBlockingQueueTests.java
 *
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 5 Nov 2018
 *
 */
public class ArrayBlockingQueueTests {

	public static void main(final String[] args) {

		final ArrayBlockingQueue<String> abq = new ArrayBlockingQueue<>(6);
		abq.add("A"); abq.add("B"); abq.add("C"); abq.add("D"); abq.add("E");
		System.out.println(abq);
		final String[] array = new String[abq.size()];
		abq.toArray(array);
		System.out.println(Arrays.toString(array));
		final List<String> list = new ArrayList<>();
		abq.drainTo(list);
		System.out.println(list);
	}
}
