/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package shop;


import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;

import javafx.util.Pair;
import main.Config;

/**
 *
 * QueueMangerTest.java
 *
 *
 * @author c0der
 * 5 Nov 2018
 *
 */
class QueueMangerTest {

	private QueuesManager queueManger;
	private CheckoutsManager checkoutsManager;
	private Config config;

	@Test
	public void test() {

		config = new Config();
		final FloorCustomers customers = new FloorCustomers(config);
		checkoutsManager = new CheckoutsManager(customers, config);
		queueManger = checkoutsManager.getQueueManager();

		testMinMaxLoadedQueues();
	}

	void testMinMaxLoadedQueues(){

		Pair<CustomerQueue, CustomerQueue> minMax = queueManger.minMaxLoadedQueues();
		assertThat("All queues are blocked, min should return null",  minMax.getKey() , nullValue());
		assertThat("All queues are blocked, max should not return null",  minMax.getValue() , notNullValue() );

		openAllQueues(); //unblock queues

		minMax = queueManger.minMaxLoadedQueues();
		assertThat("Min loaded queue should not be null",  minMax.getKey() , notNullValue() );
		assertThat("Max loaded queue should not be null",  minMax.getValue() , notNullValue() );
		assertThat("All are equally loaded, key and and value return the same",  minMax.getValue() , equalTo(minMax.getKey()));

		final List<Checkout> checkOuts = checkoutsManager.getCheckouts();
		final CustomerQueue q0 = checkOuts.get(0).getQueue();
		final CustomerQueue q1 = checkOuts.get(1).getQueue(); final int i1=0;
		final CustomerQueue q2 = checkOuts.get(2).getQueue(); int i2=0;
		final CustomerQueue q3 = checkOuts.get(3).getQueue(); int i3=0;
		final CustomerQueue q4 = checkOuts.get(4).getQueue(); int i4 = 0;

		assertThat("Queue should be empty", q4.remainingCapacity() , equalTo(q4.getQueueCapcity()) );

		//------------add 4 customers to queue 4
		q4.addCustomer(new Customer(config)); i4++;
		assertThat("Queue size should be "+i4, q4.remainingCapacity() , equalTo(q4.getQueueCapcity()- i4) );
		assertThat("Queue size should be "+i4, q4.numOfQueCustomers().get() , equalTo(q4.getQueueCapcity()- q4.remainingCapacity()) );

		minMax = queueManger.minMaxLoadedQueues();
		assertThat("q4 is max loaded queue" , minMax.getValue() , equalTo(q4) );
		assertThat("q4 shoud not be min" , minMax.getKey() , not(equalTo(q4)) );

		//--------
		q4.addCustomer(new Customer(config)); i4++;
		assertThat("Queue size should be "+i4, q4.remainingCapacity() , equalTo(q4.getQueueCapcity()- i4) );
		assertThat("Queue size should be "+i4, q4.numOfQueCustomers().get() , equalTo(q4.getQueueCapcity()- q4.remainingCapacity()) );

		minMax = queueManger.minMaxLoadedQueues();
		assertThat("q4 is max loaded queue" , minMax.getValue() , equalTo(q4) );
		assertThat("q4 shoud not be min" , minMax.getKey() , not(equalTo(q4)) );

		//--------
		q4.addCustomer(new Customer(config)); i4++;
		assertThat("Queue size should be "+i4, q4.remainingCapacity() , equalTo(q4.getQueueCapcity()- i4) );
		assertThat("Queue size should be "+i4, q4.numOfQueCustomers().get() , equalTo(q4.getQueueCapcity()- q4.remainingCapacity()) );

		minMax = queueManger.minMaxLoadedQueues();
		assertThat("q4 is max loaded queue" , minMax.getValue() , equalTo(q4) );
		assertThat("q4 shoud not be min" , minMax.getKey() , not(equalTo(q4)) );

		//--------
		q4.addCustomer(new Customer(config)); i4++;
		assertThat("Queue size should be "+i4, q4.remainingCapacity() , equalTo(q4.getQueueCapcity()- i4) );
		assertThat("Queue size should be "+i4, q4.numOfQueCustomers().get() , equalTo(q4.getQueueCapcity()- q4.remainingCapacity()) );

		minMax = queueManger.minMaxLoadedQueues();
		assertThat("q4 is max loaded queue" , minMax.getValue() , equalTo(q4) );
		assertThat("q4 shoud not be min" , minMax.getKey() , not(equalTo(q4)) );

		//------------add 3 customers to queue 3 || q4 has 4
		q3.addCustomer(new Customer(config)); i3++;
		assertThat("Queue size should be "+i3, q3.remainingCapacity() , equalTo(q3.getQueueCapcity()- i3) );
		assertThat("Queue size should be "+i3, q3.numOfQueCustomers().get() , equalTo(q3.getQueueCapcity()- q3.remainingCapacity()) );

		minMax = queueManger.minMaxLoadedQueues();
		assertThat("q4 is max loaded queue" , minMax.getValue() , equalTo(q4) );
		assertThat("q4 shoud not be min" , minMax.getKey() , not(equalTo(q4)) );
		assertThat("q3 shoud not be min" , minMax.getKey() , not(equalTo(q3)) );

		//------
		q3.addCustomer(new Customer(config)); i3++;
		assertThat("Queue size should be "+i3, q3.remainingCapacity() , equalTo(q3.getQueueCapcity()- i3) );
		assertThat("Queue size should be "+i3, q3.numOfQueCustomers().get() , equalTo(q3.getQueueCapcity()- q3.remainingCapacity()) );

		minMax = queueManger.minMaxLoadedQueues();
		assertThat("q4 is max loaded queue" , minMax.getValue() , equalTo(q4) );
		assertThat("q4 shoud not be min" , minMax.getKey() , not(equalTo(q4)) );
		assertThat("q3 shoud not be min" , minMax.getKey() , not(equalTo(q3)) );

		//------
		q3.addCustomer(new Customer(config)); i3++;
		assertThat("Queue size should be "+i3, q3.remainingCapacity() , equalTo(q3.getQueueCapcity()- i3) );
		assertThat("Queue size should be "+i3, q3.numOfQueCustomers().get() , equalTo(q3.getQueueCapcity()- q3.remainingCapacity()) );

		minMax = queueManger.minMaxLoadedQueues();
		assertThat("q4 is max loaded queue" , minMax.getValue() , equalTo(q4) );
		assertThat("q4 shoud not be min" , minMax.getKey() , not(equalTo(q4)) );
		assertThat("q3 shoud not be min" , minMax.getKey() , not(equalTo(q3)) );

		//------------add 2 customers to queue 2 || q4 has 4 || q3 has 3
		q2.addCustomer(new Customer(config)); i2++;
		assertThat("Queue size should be "+i2, q2.remainingCapacity() , equalTo(q2.getQueueCapcity()- i2) );
		assertThat("Queue size should be "+i2, q2.numOfQueCustomers().get() , equalTo(q2.getQueueCapcity()- q2.remainingCapacity()) );

		minMax = queueManger.minMaxLoadedQueues();
		assertThat("q4 is max loaded queue" , minMax.getValue() , equalTo(q4) );
		assertThat("q4 shoud not be min" , minMax.getKey() , not(equalTo(q4)) );
		assertThat("q3 shoud not be min" , minMax.getKey() , not(equalTo(q3)) );
		assertThat("q2 shoud not be min" , minMax.getKey() , not(equalTo(q2)) );

		//--

		q2.addCustomer(new Customer(config)); i2++;
		assertThat("Queue size should be "+i2, q2.remainingCapacity() , equalTo(q2.getQueueCapcity()- i2) );
		assertThat("Queue size should be "+i2, q2.numOfQueCustomers().get() , equalTo(q2.getQueueCapcity()- q2.remainingCapacity()) );

		minMax = queueManger.minMaxLoadedQueues();
		assertThat("q4 is max loaded queue" , minMax.getValue() , equalTo(q4) );
		assertThat("q4 shoud not be min" , minMax.getKey() , not(equalTo(q4)) );
		assertThat("q3 shoud not be min" , minMax.getKey() , not(equalTo(q3)) );
		assertThat("q2 shoud not be min" , minMax.getKey() , not(equalTo(q2)) );


		//block q4 and q1 and assert that min - max are q0 qnd q4 (calculating max ignores blocking)
		q1.setBlocked(true); q4.setBlocked(true);
		minMax = queueManger.minMaxLoadedQueues();
		assertThat("q3 is max unblocked loaded queue" , minMax.getValue() , equalTo(q4) );
		assertThat("q0 is min unblocked loaded queue" , minMax.getKey() , equalTo(q0) );

		//top up all queues
		for(final Checkout checkout : checkOuts) {

			final CustomerQueue queue = checkout.getQueue();
			queue.setBlocked(false);
			boolean added = true;
			while (added) {
				added = queue.addCustomer(new Customer(config));
			}

			assertThat("Queue should be full ", queue.remainingCapacity() , equalTo(0) );
			assertThat("Queue should be full ", queue.numOfQueCustomers().get() , equalTo(queue.getQueueCapcity()) );
		}

		minMax = queueManger.minMaxLoadedQueues();

		assertThat("All are equally loaded, key and and value return the same",  minMax.getValue() , equalTo(minMax.getKey()));
		System.out.println(minMax.getKey() +" "+ minMax.getValue());
	}

	void openAllQueues() {

		for(final Checkout checkout : checkoutsManager.getCheckouts()) {
			checkout.getQueue().setBlocked(false);
		}
	}
}
