/*
 * c0der
 * https://bitbucket.org/c0derepo/
 */
package shop;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;

import main.Config;
import wait.Wait;

/**
 *
 *ArrayBlockingQueueTest.java
 *
 * @author c0der <a href="https://bitbucket.org/c0derepo/">bitbucket.org/c0derepo</a><br/>
 * 22 Nov 2018
 *
 */
class CustomerQueueTest {

	private static final int CAPACITY = 3;
	final TimeUnit unit = TimeUnit.MILLISECONDS;
	private AssertionError ae;

	@Test
	void test() throws InterruptedException {
		putLock();
		getLock();
		transferBetweenQueues();
	}

	private void putLock() throws InterruptedException {

		final CustomerQueue aQueue = new CustomerQueue(CAPACITY);
		aQueue.setBlocked(false);
		ae = null;

		//add customers
		assertThat("Add to unlocked queue should return true ", aQueue.addCustomer(new Customer(new Config())), equalTo(true) );
		assertThat("Add to unlocked queue should return true ", aQueue.addCustomer(new Customer(new Config())), equalTo(true) );
		assertThat("Add to unlocked queue should return true ", aQueue.addCustomer(new Customer(new Config())), equalTo(true) );

		//block adding to queue
		aQueue.obtainPutLock();

		assertThat("Add to unlocked queue should return true ", aQueue.getLast(), notNullValue() );
		assertThat("Add to unlocked queue should return true ", aQueue.getNext(), notNullValue() );
		assertThat("Add to unlocked queue should return true ", aQueue.drainToList().size(), equalTo(1) );

		final Customer customer = new Customer(new Config());
		assertThat("Add to locked queue should return false ", aQueue.addCustomer(customer), equalTo(false) );
		assertThat("Add to locked queue should return false ", aQueue.addCustomer(customer,2000), equalTo(false) );
		final Thread t = new Thread (() -> {
			try{
				assertThat("Delayed add should return true ", aQueue.addCustomer(new Customer(new Config())), equalTo(true) );
			}catch(final AssertionError ex){ae = ex;}
		});
		t.start();

		aQueue.releasePutLock();
		assertThat("Add to unlocked queue should return true ", aQueue.addCustomer(customer), equalTo(true) );
		try{
			aQueue.addCustomer(customer,2000);
			fail("Add the sme customer again should throw exception ");
		}catch(final AssertionError ex){}

		t.join(3000);
		if(ae != null) throw ae;
		else {
			ae = null;
		}
	}

	private void getLock() throws InterruptedException {

		final CustomerQueue aQueue = new CustomerQueue(CAPACITY);
		aQueue.setBlocked(false);
		ae = null;

		final Thread t = new Thread (() -> {
			try{
				assertThat("Delayed add should return true ", aQueue.waitForNext(), notNullValue());
				aQueue.addCustomer(new Customer(new Config()));
			}catch(final AssertionError ex){ae = ex;}
		});
		t.start();

		//block adding to queue
		aQueue.obtainTakeLock();

		//add customers
		assertThat("Add to take-locked queue should return true ", aQueue.addCustomer(new Customer(new Config())), equalTo(true) );
		assertThat("Add to take-locked queue should return true ", aQueue.addCustomer(new Customer(new Config())), equalTo(true) );
		assertThat("Add to take-locked queue should return true ", aQueue.addCustomer(new Customer(new Config())), equalTo(true) );

		assertThat("Take from locked queue should return false ", aQueue.getLast(), equalTo(null) );
		assertThat("Take from locked queue should return false ", aQueue.getNext(), equalTo(null) );
		assertThat("Take from locked queue should return false ", aQueue.drainToList(), equalTo(null) );

		aQueue.releaseTakeLock();
		Wait.millis(200); //let t thread finish
		assertThat("Take from un-locked queue should return false ", aQueue.getLast(), notNullValue() );
		assertThat("Take from un-locked queue should return false ", aQueue.getNext(), notNullValue() );
		assertThat("Take from un-locked queue should return false ", aQueue.drainToList().size(), equalTo(1) );

		t.join(3000);
		if(ae != null) throw ae;
		else {
			ae = null;
		}
	}

	private void transferBetweenQueues() {

		final CustomerQueue source = new CustomerQueue(CAPACITY);
		final CustomerQueue target = new CustomerQueue(CAPACITY);
		source.setBlocked(false);
		target.setBlocked(false);
		ae = null;

		//add customers
		assertThat("Add to unlocked queue should return true ", source.addCustomer(new Customer(new Config())), equalTo(true) );
		assertThat("Add to unlocked queue should return true ", source.addCustomer(new Customer(new Config())), equalTo(true) );
		assertThat("Add to unlocked queue should return true ", source.addCustomer(new Customer(new Config())), equalTo(true) );

		//transfer all
		assertThat(CustomerQueue.transferBetweenQueues(source, target), equalTo(true) );
		assertThat(CustomerQueue.transferBetweenQueues(source, target), equalTo(true) );
		assertThat(CustomerQueue.transferBetweenQueues(source, target), equalTo(true) );

		assertThat("target shoud be full",target.numOfQueCustomers().get(), equalTo(CAPACITY) );
		assertThat("source should be empty", source.numOfQueCustomers().get(), equalTo(0) );
	}
}
